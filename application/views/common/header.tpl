<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="{DOMAIN}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{PAGE_TITLE}</title>
<link rel="stylesheet" type="text/css" href="css/global.css"/>
<link rel="stylesheet" type="text/css" href="css/validation_live.css"/>
<link rel="stylesheet" type="text/css" href="css/redmond/jquery-ui-1.10.2.custom.css"/>
{ADDCSS}
<!--[if IE]>
    <link type="text/css" rel="stylesheet" href="css/styles-ie.css" />
<![endif]-->
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.corner.js"></script>
<script type="text/javascript" src="js/livevalidation_standalone.compressed.js"></script>
{ADDJS}
<script>
$(document).ready(function(e){
      var liste_forfaits="";
		var one_forfait="";
		var forfait_data="";
		var res_data="";
		var num_dest="";
		var id_pay="";
		var nom_forfait="";
//---------------------
$("#numero_ab_conn").focus();

$("#new_equipement,#equipement").val("{device}");

//-----------------
function showLoading(_text){
	$(".loading").text(_text).fadeIn();
}

//------------------
function hideLoading(){
	$(".loading").fadeOut();
}

//--------------------
$(".actions a").corner("10px tr").corner("5px bl");
$(".forfaits").corner("10px tr").corner("10px bl");
	/*$(".btn_no_forfait,.submit_spons").corner("15px tl").corner("15px tr").corner("15px br");*/
	$(".titleresumeCompte,.titleresumeCompte3,.titleresumeCompte2").corner("5px tl")
	                    .corner("5px br");


//-----------------------
$(".stop_forfait").click(function(e) {
    e.preventDefault();
	if(confirm("VOULEZ VOUS VRAIMENT SUPPRIMER VOTRE FORFAIT ACTIF?")){
		showLoading("Chargement en cours....");
		window.location.href="index.php?ctrl=operations&action=stop";
	}
});

//----------------------
$("a:not(.forfait_buy,.offer_forfaits_buy,.stop_forfait),.btn_no_forfait,.submit_spons").click(function(e) {
	
	
  if($(this).attr("href")===undefined){
	   showLoading("Chargement en cours...");
  }else{

	  if($(this).attr("href")!="#"){
		   showLoading("Chargement en cours...");
	  }
  }
  //cas special des boutons dinscriptions
  if($(this).hasClass("ins_launch")){
	  
	  if($(".LV_invalid").length){
		//  alert("iiiii");
	  hideLoading();
	  }
  }
  //----------------------
  $(".ins_3g").submit(function(e) {
      if($(".LV_invalid").length){
		//  alert("iiiii");
	  hideLoading();
	  }
});
  
  //-------------------
  

});

//---------------
	var ratio={CONSO_RATIO};
	$("#conso_bar").progressbar(
	{value:ratio}
	);
    var progress_value=$("#conso_bar").progressbar("value");
	if(parseInt(progress_value)<=50) $("#conso_bar .ui-progressbar-value").addClass("mid_conso");
	if(parseInt(progress_value)<=20){
		$("#conso_bar .ui-progressbar-value").removeClass("mid_conso").addClass("limit_conso");
	}


	//--------------------------------------
	$(".offer_forfaits_buy").live("click",function(e){
		e.preventDefault();
		 num_dest=$("#dest_numero").val();
		if(num_dest==""){
			alert("Veuillez saisir le numéro du bénéficiaire");
		}else if($("#equipement").val()=="-1"){
		    alert("Veuillez selectionnner un équipement");
		}else{
			  
		 id_pay=$(this).attr("ref");
		nom_forfait=$(this).closest(".forfaits").find(".forfait_nom").text();
		//var prix_forfait=$(this).closest(".forfaits").find(".forfait_nom").text();
		if(confirm("VOULEZ VOUS ENVOYER LE "+nom_forfait.toUpperCase()+" AU NUMERO +225"+num_dest+" ?")){
			//$(document).scrollTop(0);
			//showLoading("Chargement en cours...");
			//window.location.href="?ctrl=operations&action=send_forfait&ref="+id_pay+"&dest="+num_dest+"&nom_forf="+nom_forfait;

			//------------------
			$("#nom_forfait").val(nom_forfait);
		$("#ref_forfait").val(id_pay);
		$(".conf_send").fadeIn();
		$(".confirm_send").text("Veuillez saisir votre mot de passe afin de confirmer l' envoi du forfait "+nom_forfait+" au numéro +225"+num_dest).fadeIn();

			//-----------------
		}
		}
	});
     //---------------------------
	$("#equipement").change(function(e) {


        $(".load_forf").fadeIn();
		 num_dest=$("#dest_numero").val();
		var equipement=$("#equipement").val();
		//alert(equipement);
		$.post("index.php?ctrl=operations&action=getOffres",
		{device:equipement},
		function(data){
		//	alert(data);
		res_data="";
			$(".load_forf").fadeOut();
				var list_offres=data.split("@@@");
			for(var i=0;i<list_offres.length;i++){
		    one_forfait=list_offres[i];
			forfait_data=one_forfait.split("@");
			res_data+= '<div class="forfaits">';
     res_data+='<div class="forfait_nom">'+forfait_data[1]+'</div>';
     res_data+='<a href="#" class="offer_forfaits_buy" ref="'+forfait_data[0]+'">offrir</a>';
res_data+='<div class="forfait_decor"><img src="img/forfaits/ait_decoration.png" width="53" height="86" /></div>';
res_data+='<p class="infos_forfaits">Validité :'+forfait_data[2]+' jour(s)</p>';
res_data+='<p class="infos_forfaits">Volume Max :'+forfait_data[3]+'</p>';
res_data+='<p class="forfait_prix">Prix : '+forfait_data[4] +' FCFA</p></div>';


			}
			//alert(res_data);
			$("#liste_forfaits").html(res_data);
			$("#liste_forfaits").hide().fadeIn('slow');




			//----------------------------
		});
    });


	//--------------------
$(".forfait_buy").click(function(e) {
        e.preventDefault();
		 id_pay=$(this).attr("ref");
		nom_forfait=$(this).closest(".forfaits").find(".forfait_nom").text();
		//var prix_forfait=$(this).closest(".forfaits").find(".forfait_nom").text();
		if(confirm("VOULEZ VOUS VRAIMENT SOUSCRIRE AU "+nom_forfait.toUpperCase()+"?")){
			 showLoading("Chargement en cours...");
			window.location.href="index.php?ctrl=operations&action=add_forfait&ref="+id_pay;

		}
    });

//-------------------

});

</script>
</head>

<body>
<base href="{DOMAIN}">
<div class="header">
  <div class="logo"><img src="img/logo.gif" width="116" height="96" /></div>
</div>
