{HEADER}
<div class="container_moov2">
  <div class="menu_operation">

  {MENU}
  </div>
  <div class="menu_infos">
  <div class="{ERROR_TYPE}">{ERROR_MESSAGE}</div>
  <h1 class="titleresumeCompte"> Mon solde</h1>

  <div class="contentBloc">
  <p class="arial13 infos_cons">solde disponible sur le compte : <strong>{SOLDE} FCFA</strong></p>
  </div>
   <h1 class="titleresumeCompte"> Forfait actif</h1>

  <div class="contentBloc">
  <!-- BEGIN FORFAITS_INFOS -->
  <p class="arial13 infos_cons {hide_show_forfait}">{FORFAITS_INFOS.name}<strong class="retract">{FORFAITS_INFOS.valeur}</strong></p>
  <!-- END FORFAITS_INFOS -->


  <p class="arial13 no_forfaits {hide_no_forfait} infos_cons">Vous n' avez aucun forfait en cours</p>

  </div>
  </div>
</div>
</body>
</html>