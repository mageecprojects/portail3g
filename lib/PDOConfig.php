<?php

/**
 *
 *
 * @version $Id$
 * @copyright 2010
 */
class PDOConfig extends PDO {

private $engine;
private $host;
private $database;
private $user;
private $pass;



public function __construct($lengine,$lhost,$ldatabase,$luser,$lpass){
	$this->engine = $lengine;
	$this->host = $lhost;
	$this->database = $ldatabase;
	$this->user = $luser;
	$this->pass = $lpass;
	$dns = $this->engine.':dbname='.$this->database.";host=".$this->host;
	parent::__construct( $dns, $this->user, $this->pass );
	$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



}

	public function _query($sql){
		$list=$this->prepare($sql);
		$list->execute();
		$result=$list->fetchAll(PDO::FETCH_ASSOC);
		$list->closeCursor();
		return $result;
	}

	//--------------------------
	public function _queryOne($sql){

		$list=$this->prepare($sql);
		$list->execute();
		$result=$list->fetch(PDO::FETCH_ASSOC);
		$list->closeCursor();
		if(empty($result)) $result=array();
		return $result;
	}


	//--------------------------
	public function executeRequete($sql,$debug=0){
		try{
			$list=$this->prepare($sql);
			$list->execute();
			$result=$list->fetchAll(PDO::FETCH_ASSOC);
			$list->closeCursor();
		}catch (PDOException $e){
			if(getenv("APPLICATION_ENV")!="PRODUCTION"){
			echo "reket=".$sql."<br>retour=".$e->getMessage();
            }

		}

		if($debug==1){
           	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
			echo "reket=".$sql."<br>retour=".print_r($result,true);
           	}
		}

		return $result;
	}

	public function execCustom($sql,$debug=0){
		try{
		$result=$this->exec($sql);
		}catch(PDOException $e){

			if(getenv("APPLICATION_ENV")!="PRODUCTION"){
				echo "reket=".$sql."<br>retour=".$e->getMessage();
			}
		}
		if($debug==1){
      	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
			echo "reket=".$sql."<br>retour=".print_r($result,true);
          	}
		}

	}
	public function executeOne($sql,$debug=0){
     $error="";
		try{
			$list=$this->prepare($sql);
			$list->execute();
			$result=$list->fetch(PDO::FETCH_ASSOC);
			$list->closeCursor();
			if(empty($result)) $result=array();
		}catch(PDOException $e){
          	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
		echo "reket=".$sql."<br>retour=".$e->getMessage();
         	}

		}
			if($debug==1){
              	if(getenv("APPLICATION_ENV")!="PRODUCTION"){
				echo "reket=".$sql."<br>retour=".print_r($result,true);
              	}
			}


		return $result;
	}

}
?>