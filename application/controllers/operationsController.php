<?php
require_once("baseController.php");

class operationsController extends baseController{

   public function index(){
   // print_r($_SESSION);
   	$connected=$this->status_connexion();
   	$consommation_bas_debit="(Consommation bas débit)";
   	$mode_consommation="";
    $is_bas_debit=false;
   	if(!$connected){
   	//	echo "pppppp";
   		Tools::redirect($this->domain.$this->login_url);
   	}else{


   	    $reponse=new Moov3GResponseStatutAbonne();
   		$solde_obj=new Moov3GResponseConsultationCompte();
   		$hist_obj=new Moov3GResponseHistoriqueSouscription();
   		$client=new Moov3GSoapWebsiteClient();

   	$reponse=$client->StatutAbonne(Tools::getsession("id_conn"));

   		if(Tools::getsession("num_conn")=="42689986"){
   		//	print_r($_SESSION);

   		}

   	//	print_r($reponse);
   	$solde_obj=$client->ConsultationCompte(Tools::getsession("id_conn"));
   		$hist_obj=$client->HistoriqueSouscription(Tools::getsession("id_conn"),1,1);
         // print_r($solde_obj);
   		if(!$reponse->Succes){
   				Tools::redirect($this->domain.$this->login_url);
   		}else{
   		//
   		//	print_r($reponse);
   			$this->vue->assign_var("SOLDE",Tools::formatfrechprice($solde_obj->Montant ));
            $equipement=Tools::getsession("equipement");
   			if($reponse->ForfaitEnCours){// ila un forfait
   			//	echo "kkkkkkkkkkkkk";


   				$this->vue->set_filenames(array("forfaits"=>"operations/hasforfait.tpl"));
                $volume_tot=$this->ConvertVolumeWithoutUnit($reponse->VolumeTotal);
                $volume_used=$this->ConvertVolumeWithoutUnit($reponse->VolumeConsomme);
                $volume_rest=floatval($volume_tot-$volume_used);
   				$ratio_cons=$this->ConvertConsoRatio($volume_tot,$volume_rest );

   				if($volume_rest<0){// cas d' une consommation bas débit

   					$volume_rest=0;
   					$ratio_cons=0;
   					$is_bas_debit=true;

   				}
   			//	print_r($_SESSION);
   				if(Tools::getsession("num_conn")=="01022728"){

   					echo $reponse->VolumeTotal."<br>";
   					echo $reponse->VolumeConsomme."<br>";
   					echo $this->ConvertVolumeWithoutUnit($reponse->VolumeConsomme)."<br>";
   					echo $volume_used."<br>";
   					echo $this->ConvertVolumeWithoutUnit($reponse->VolumeTotal)."<br>";
   					echo $volume_rest."<br>";
   					echo $ratio_cons."<br>";
   					echo (($volume_rest/$volume_tot)*100)."<br>";
   				//	echo number_format($volume_rest, 2, ',', ' ');
   				//	$this->getLastConnectionDate(1);

   				}

   				if($is_bas_debit) $mode_consommation=$consommation_bas_debit;
   			 //on cache la div
   				$this->vue->assign_vars(array(
   				"CONSO_RATIO"=>$ratio_cons,
   				"MAX_VOLUME"=>Tools::formatfrechprice($volume_tot),
   				"FORFAIT_ACTIF"=>$reponse->NomForfait." ".$mode_consommation,
   				"date_expiration"=>Tools::convert_date_to_french($reponse->DateExpiration),
   				"volume_total"=>$this->ConvertVolume($reponse->VolumeTotal),
   				"volume_consomme"=>Tools::formatfrechprice($volume_used,2),
   				"volume_dispo"=>Tools::formatfrechprice($volume_rest,2)

   				)

					);


   			}else{
   				$this->vue->set_filenames(array("forfaits"=>"operations/no_forfaits.tpl"));

   			}

   			$this->vue->set_filenames(array("body"=>"operations/dashboard.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl"));
   			if(empty($hist_obj->Elements)){
   				$dernier_forfait="Aucun forfait souscrit";
   				$dernier_forfait_date="Aucune souscription";

   			}else{
   				$dernier_forfait=$hist_obj->Elements[0]->NomForfait;
   				$dernier_forfait_date=$hist_obj->Elements[0]->DateSouscription;
   				$dernier_forfait_date=Tools::convert_date_to_french($dernier_forfait_date);
   			}

   		//	echo "uuuu".$dernier_forfait;

   			$this->vue->assign_var("LAST_FORFAIT",$dernier_forfait );//on cache la div
   			$this->vue->assign_var("DATE_LAST_CONNEXION",$this->getLastConnectionDate());//on cache la div
   			$this->vue->assign_var("menu_underline_compte","underline" );//on cache la div
   			$this->vue->assign_var("DATE_LAST_SOUSCRIPTION",$dernier_forfait_date);//on cache la div
   		//	$this->vue->assign_var("device",$this->device_list[$equipement]);//on cache la div

   			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
   			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
   			$this->vue->assign_var_from_handle("LOGIN","login" );
   			$this->vue->assign_var_from_handle("MENU","menu" );
   			$this->vue->assign_var_from_handle("HEADER","header");
   			 $this->vue->assign_var_from_handle("FORFAITS","forfaits" );
   			$this->vue->pparse("body");
   		}

   	}



   }

    //-----------------------------------------------------
	function testdevice(){

		if($this->isMobileDevice()){
			echo "mobile/tablette";
		}else{
			echo "pc";
		}

	}

	//----------------------------------------------------
	function abonneredirection(){
		Tools::redirect($this->domain.$this->login_url);
	//	echo $_GET["num"];
	}


	//-------------------------------------------------



	//------------------------------------------
	function activatepayg(){

		$num=Tools::getQueryValue("num");
		if($num=="") {
			Tools::redirect($this->domain.$this->login_url);
		}else{
      //  echo "uuuuuu";
			$client=new Moov3GSoapWebsiteClient();
			$reponse=new Moov3GResponseActivatePAYG();
			$reponse=$client->ActivatePAYG($num);

		//	if($reponse->Succes){
				$this->addMessage($this->MES_SUCESS,utf8_encode($reponse->MessageSucces) );
		/*	}else{
				$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur) );
			}*/
		//	var_dump($reponse);
			$this->vue->set_filenames(array("header"=>"common/header.tpl","body"=>"operations/redirection.tpl"));

				$this->vue->assign_var("FLAG_SHOW_MENU_REDIRECTION","hide");


			$this->vue->assign_var("msisdn",$num );

			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->pparse("body");
		}
	}



	//------------------------------------------------

	function checkpendingsouscription(){
		$num=$_SESSION["pending_num"];
			echo $num;
		if($num=="") {
			Tools::redirect($this->domain.$this->login_url);
		}else{

			//	Tools::redirect($this->domain.$this->login_url);
			$client=new Moov3GSoapWebsiteClient();
			$reponse=new Moov3GResponseActiveBundlePending();

			$this->vue->set_filenames(array("header"=>"common/header.tpl","body"=>"operations/redirection.tpl"));
			$reponse=$client->ConfirmBundlePending($num);
			//var_dump( $reponse);

			if($reponse->Succes){
				$this->addMessage($this->MES_SUCESS,$reponse->MessageSucces);
				$show_menu_redirection="hide";
				unset($_SESSION["pending_num"]);
			}else{
				$this->addJs(array("reload.js"));
				$this->addMessage($this->MES_ERROR,$reponse->MessageErreur);
				$show_menu_redirection="hide";
			}

			$this->vue->assign_var("FLAG_SHOW_MENU_REDIRECTION",$show_menu_redirection );
			$this->vue->assign_var("msisdn",$num );

			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->pparse("body");

		}
	}


	//------------------------------------------------

	function abonneredirection2(){
		$num=Tools::getQueryValue("num");
	//	echo $num;
		if($num=="") {
			Tools::redirect($this->domain.$this->login_url);
		}else{

	//	Tools::redirect($this->domain.$this->login_url);
		$client=new Moov3GSoapWebsiteClient();
		$reponse=new Moov3GResponseActiveBundlePending();

	//	$this->vue->set_filenames(array("header"=>"common/header.tpl","body"=>"operations/redirection.tpl"));
	$reponse=$client->ActiveBundlePending($num);
			if($reponse->Succes){
					$this->vue->set_filenames(array("header"=>"common/header.tpl","body"=>"operations/redirection.tpl"));
				$this->addMessage($this->MES_SUCESS,"VOUS NE DISPOSEZ D' AUCUN FORFAIT INTERNET OU VOTRE FORFAIT INTERNET EST EPUISE.VEUILLEZ SELECTIONNEZ VOTRE MODE DE NAVIGATION" );
				$this->vue->assign_var("FLAG_SHOW_MENU_REDIRECTION",$show_menu_redirection );
				$this->vue->assign_var("msisdn",$num );
			}else{
				$_SESSION["pending_num"]=$num;
				Tools::redirect($this->domain."account/pending/");
			}
 // var_dump( $reponse);
       // $codereponse=intval($reponse->LastConfigNode["child"][4]["content"]);
       // $statut=$reponse->LastConfigNode["child"][9]["content"];
       // $codereponse=$reponse->LastConfigNode["child"][4]["content"];
		//	echo $codereponse."<br>";
		//	echo $statut."<br>";
	//	var_dump($reponse->LastConfigNode["child"]);

	/*	if((intval($codereponse)==0)&&($statut=="true")){
			$this->addMessage($this->MES_SUCESS,$reponse->MessageSucces );
			$show_menu_redirection="hide";
		}elseif(($statut=="false") &&(($codereponse==-1)||($codereponse==-3)||($codereponse==-4))){
			$this->addMessage($this->MES_ERROR,$reponse->MessageErreur );
			$show_menu_redirection="hide";
		}elseif(($statut=="false")&&($codereponse==-2)){
			$show_menu_redirection="";
			$this->addMessage($this->MES_SUCESS,"VOTRE FORFAIT DE NAVIGATION INTERNET EST EPUISE" );
		}else{
			$show_menu_redirection="";
			$this->addMessage($this->MES_SUCESS,"VOTRE FORFAIT DE NAVIGATION INTERNET EST EPUISE" );
		}*/
	//	var_dump($reponse);
		//	echo $_GET["num"];

		/*	$this->vue->assign_var("FLAG_SHOW_MENU_REDIRECTION",$show_menu_redirection );
			$this->vue->assign_var("msisdn",$num );*/

			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->pparse("body");

		}
	}


	//----------------------------------------
	function logout(){


		$reponse=new Moov3GResponseDeconnexionMembre();
		$client=new Moov3GSoapWebsiteClient();
		$reponse=$client->DeconnexionMembre(Tools::getsession("id_conn"));
		if($reponse->Succes){
			session_unset();
			session_destroy();
				Tools::redirect($this->domain.$this->login_url);
		}else{
		//	$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");
			session_unset();
			session_destroy();
			Tools::redirect($this->domain.$this->login_url);
		//	Tools::redirect($this->dashbord_url);
		}


	}
    //--------------------------------------------------
	public function forfaits(){
          $reponse_offres=new Moov3GResponseListeOffre();
		$reponse_solde=new Moov3GResponseConsultationCompte();
		$reponse_statut=new Moov3GResponseStatutAbonne();
		$client=new Moov3GSoapWebsiteClient();
		$nom_forfait_en_cours="Aucun forfait actif sur ce compte";
		$connected=$this->status_connexion();

		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);
		}else{

     //  echo Tools::getsession("id_conn");
		  $reponse_offres=$client->ListeOffre(Tools::getsession("id_conn"),Tools::getsession("equipement"));
			$reponse_statut=$client->StatutAbonne(Tools::getsession("id_conn"));
			$reponse_solde=$client->ConsultationCompte(Tools::getsession("id_conn"));
			if(!$reponse_statut->Succes){
				Tools::redirect($this->domain.$this->login_url);
			}else{
				//	print_r($reponse_offres);
				if($reponse_statut->ForfaitEnCours){
					$nom_forfait_en_cours=$reponse_statut->NomForfait;
					$date_forfait="Expire le : <strong>".Tools::convert_date_to_french($reponse_statut->DateExpiration)."</strong><br><br>";
				}else{
					$show_stop_forfait="hide";
					$date_forfait="";
				}
			}

			//on charge les forfaits
			$offres_listes=$this->filterForfaits($reponse_offres->Elements,Tools::getsession("equipement") );
			foreach($offres_listes as $offre){
				$this->vue->assign_block_vars("offres",
				array("nom"=>strtoupper($offre->Nom),
				"duree"=>$offre->TotalJours,
				"id"=>$offre->Id,
				"volume"=>$this->ConvertVolumeOffre($offre->VolumeMax),
				"prix"=>Tools::formatfrechprice($offre->Montant),
			)
					 );
			}

			//---------on assigne les vues aux variables

			$this->vue->assign_var("SOLDE",Tools::formatfrechprice($reponse_solde->Montant ));
			$this->vue->assign_var("menu_underline_forfait","underline" );//on cache la div

			$this->vue->assign_vars(array(
		    "forfait_actif"=>$nom_forfait_en_cours,
		    "date_forfait"=>$date_forfait,
		    "flag_show_date_forfait"=>$show_date_forfait,
		    "flag_stop_forfait"=>$show_stop_forfait,
		    ));
			$this->vue->set_filenames(array("body"=>"operations/offres.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl","forfaits"=>"operations/liste_offres.tpl"));
			$this->vue->assign_var_from_handle("LOGIN","login" );
			$this->vue->assign_var_from_handle("MENU","menu" );
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->assign_var_from_handle("OFFRES","forfaits" );
			$this->vue->pparse("body");



		}







	}
	//---------------------------------------------------
	public function getOffres(){
		$connected=$this->status_connexion();
		$res=array();
		if(!$connected){
			echo "ERROR";
		}else{
			$device=Tools::getPostValue("device");
			$reponse=new Moov3GResponseListeOffre();
			$offre=new Moov3GOffre();
			$client=new Moov3GSoapWebsiteClient();
		//	$reponse=$client->ListeOffre(Tools::getsession("id_conn"),$this->device_list_soap[$device]);
			$reponse=$client->ListeOffre(Tools::getsession("id_conn"),$device);

			if(!$reponse->Succes){
				echo "ERROR";
			}else{
				$liste_offres=$this->filterForfaits($reponse->Elements,$device );
				foreach($liste_offres as $offre){
					$data_offres=array();
					$data_offres[]=$offre->Id;
					$data_offres[]=strtoupper($offre->Nom);
					$data_offres[]=$offre->TotalJours;
					$data_offres[]=$this->ConvertVolumeOffre($offre->VolumeMax);
					$data_offres[]=Tools::formatfrechprice($offre->Montant);
					$one_forfait=implode("@",$data_offres);
					$res[]=$one_forfait;
				}

				echo implode("@@@",$res);

			}
		}
	}


	//-----------------------------------------------


	public function stop(){
		$connected=$this->status_connexion();
		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);
		}else{
			$reponse=new Moov3GResponseStopSouscription();
			$client=new Moov3GSoapWebsiteClient();
		$reponse=$client->StopSouscription(Tools::getsession("id_conn"));
			if($reponse->Succes){
				$this->addMessage($this->MES_SUCESS,"Votre forfait a été arrêté avec succés.Vous ne disposez d' aucun forfait actif sur ce compte.");
			}else{
				$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");

			}

			Tools::redirect($this->domain.$this->dashbord_url);
		}

	}
	public function offrir_forfaits(){
		$reponse_offres=new Moov3GResponseListeOffre();
		$reponse_solde=new Moov3GResponseConsultationCompte();
		$reponse_statut=new Moov3GResponseStatutAbonne();
		$client=new Moov3GSoapWebsiteClient();
    	$nom_forfait_en_cours="Aucun forfait actif sur ce compte";
		$connected=$this->status_connexion();

		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);
		}else{


			$reponse_offres=$client->ListeOffre(Tools::getsession("id_conn"),Tools::getsession("equipement"));
			$reponse_statut=$client->StatutAbonne(Tools::getsession("id_conn"));
			$reponse_solde=$client->ConsultationCompte(Tools::getsession("id_conn"));
			if(!$reponse_offres->Succes){
				Tools::redirect($this->domain.$this->login_url);
			}else{
				//print_r($reponse_offres);
				if($reponse_statut->ForfaitEnCours){
					$nom_forfait_en_cours=$reponse_statut->NomForfait;
					$date_forfait="Expire le : <strong>".Tools::convert_date_to_french($reponse_statut->DateExpiration)."</strong><br><br>";
				}else{
					$show_stop_forfait="hide";
					$date_forfait="";
				}
			}



			//on charge les forfaits
			$offres_listes=$this->filterForfaits($reponse_offres->Elements,"0" );
			foreach($offres_listes as $offre){
				$this->vue->assign_block_vars("offres",
				array("nom"=>strtoupper($offre->Nom),
				"duree"=>$offre->TotalJours,
				"id"=>$offre->Id,
				"volume"=>$this->ConvertVolumeOffre($offre->VolumeMax),
				"prix"=>Tools::formatfrechprice($offre->Montant),
			)
				);
			}

			//---------on assigne les vues aux variables
			$this->vue->assign_var("SOLDE",Tools::formatfrechprice($reponse_solde->Montant ));
			$this->vue->assign_vars(array(
			"forfait_actif"=>$nom_forfait_en_cours,
			"date_forfait"=>$date_forfait,
			"flag_show_date_forfait"=>$show_date_forfait,
			"flag_stop_forfait"=>$show_stop_forfait,
		));
			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
			$this->vue->assign_var("menu_underline_send","underline" );//on cache la div

			$this->vue->set_filenames(array("body"=>"operations/offrir_forfait.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl","forfaits"=>"operations/liste_offres.tpl"));
			$this->vue->assign_var_from_handle("LOGIN","login" );
			$this->vue->assign_var_from_handle("MENU","menu" );
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->pparse("body");



		}







	}

	//----------------------------------------------------
	public function charge_compte_processing(){
		$connected=$this->status_connexion();
		$reponse=new Moov3GResponseRechargementCompte();
		$client=new Moov3GSoapWebsiteClient();
			$rechargement=Tools::getPostValue("code_recharge");
		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);


		}elseif($rechargement==""){

		$this->addMessage($this->MES_ERROR,"Veuillez saisir un code de rechargement" );
		Tools::redirect($this->domain.$this->recharge_url);
		}elseif(!is_numeric($rechargement)){

			$this->addMessage($this->MES_ERROR,"le format du code de rechargement est incorrecte");
			Tools::redirect($this->domain.$this->recharge_url);

		}elseif((strlen($rechargement)!=14)||(strlen($rechargement)!=16)){


			$this->addMessage($this->MES_ERROR,"le code de rechargement doit comporter 14 ou 16 chiffres");
			Tools::redirect($this->domain.$this->recharge_url);

		}else{

		//	$reponse=$client->RechargementCompte(Tools::getsession("id_conn"),'xxxxxxxxxxxxxx' );
			$reponse=$client->RechargementCompte(Tools::getsession("id_conn"),$rechargement );
			if($reponse->Succes){
				$this->addMessage($this->MES_SUCESS,"Votre rechargement s' est déroulé avec succès." );
			}else{
				$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur)."");
			}

		//	print_r($reponse);
			Tools::redirect($this->domain.$this->dashbord_url);
		}

	}
	//----------------------------------------
	public function change_password_processing(){
		$connected=$this->status_connexion();
		$reponse=new Moov3GResponseChangePassword();
		$client=new Moov3GSoapWebsiteClient();
		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);
		}else{

			$old_pass=Tools::getPostValue("old_password");
			$new_pass=Tools::getPostValue("new_password");
			$conf_pass=Tools::getPostValue("conf_password");
			if($new_pass!=$conf_pass){
					$this->addMessage($this->MES_ERROR,"Vos 2 mots de passe ne sont pas identiques.Veuillez les ressaisir.");
				Tools::redirect($this->domain.$this->change_password_url);
			}else if(($old_pass=="")||($new_pass=="")||($conf_pass=="")){

				$this->addMessage($this->MES_ERROR,"Un ou plusieurs champs sont vides.Veuillez remplir tous les champs avant de valider le formulaire.");
            	Tools::redirect($this->domain.$this->change_password_url);

			}else{




			//	$reponse=$client->RechargementCompte(Tools::getsession("id_conn"),'xxxxxxxxxxxxxx' );
			$reponse=$client->ChangePassword(Tools::getsession("num_conn"),$old_pass,$new_pass );
			if($reponse->Succes){
				$num_conn=Tools::getsession("num_conn");
				$this->database->execCustom("update 3g_inscrits set pass_word='$new_pass' where numero='$num_conn'");
				$_SESSION["password_conn"]=$new_pass;
				$this->addMessage($this->MES_SUCESS,"Le changement de votre mot de passe s' est déroulé avec succès. MOOV CI vous remercie de votre confiance." );
					Tools::redirect($this->domain.$this->dashbord_url);
			}else{
				$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur)."." );
				Tools::redirect($this->domain.$this->change_password_url);

			}

			//	print_r($reponse);

				}
		}

	}
	//----------------------------------------
	public function change_device_processing(){
		$connected=$this->status_connexion();
		$reponse=new Moov3GResponseModifieMembre();
		$infos=new Moov3GResponseInfoMembre();
		$client=new Moov3GSoapWebsiteClient();
		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);
		}else{

			$new_equipement=Tools::getPostValue("new_equipement");
			$infos=$client->InfoMembre(Tools::getsession("id_conn"));
			if($infos->Succes){

				$nom=$infos->Nom;
				$prenom=$infos->Prenoms;
				$email=$infos->Email;
				if($nom=="") $nom="ABONNE";
				if($prenom=="") $prenom="SANS NOM";
				$reponse=$client->ModifieMembre(Tools::getsession("id_conn"),$email,$nom,$prenom,$new_equipement );
				if($reponse->Succes){
					$num_conn=Tools::getsession("num_conn");
					$this->database->execCustom("update 3g_inscrits set equipement='$new_equipement' where numero='$num_conn'");
					$_SESSION["equipement"]=$new_equipement;
					$this->addMessage($this->MES_SUCESS,"Le changement de votre mot equipement s' est déroulé avec succès. MOOV CI vous remercie de votre confiance." );
					Tools::redirect($this->domain.$this->dashbord_url);
				}else{
					$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur)."." );
					Tools::redirect($this->domain.$this->change_password_url);

				}

			}else{




				//	$reponse=$client->RechargementCompte(Tools::getsession("id_conn"),'xxxxxxxxxxxxxx' );


				//	print_r($reponse);

			}
		}

	}

	///------------------------------
	public function change_name_processing(){

		$connected=$this->status_connexion();
		$reponse=new Moov3GResponseModifieMembre();
		$infos=new Moov3GResponseInfoMembre();
		$client=new Moov3GSoapWebsiteClient();
		$new_nom=Tools::getPostValue("nom_user");
		$new_prenom=Tools::getPostValue("prenom_user");
		$nompattern="/^[A-Za-z\- ]+$/i";
		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);

		}elseif(!preg_match($nompattern,$new_nom )){

		$this->addMessage($this->MES_ERROR,"Votre nom contient des caractères incorrectes" );
		Tools::redirect($this->domain.$this->change_password_url);
		}elseif(!preg_match($nompattern,$new_prenom)){

			$this->addMessage($this->MES_ERROR,"Votre prénom contient des caractères incorrectes" );
		Tools::redirect($this->domain.$this->change_password_url);
		}else{

		     $new_nom=strtoupper($new_nom);
		     $new_prenom=strtoupper($new_prenom);
			$infos=$client->InfoMembre(Tools::getsession("id_conn"));
			if($infos->Succes){

			//	$nom=$infos->Nom;
			//	$prenom=$infos->Prenoms;
				$email=$infos->Email;
				$equipement=$infos->Equipement;
			//	if($nom=="") $nom="ABONNE";
			//	if($prenom=="") $prenom="SANS NOM";
				$reponse=$client->ModifieMembre(Tools::getsession("id_conn"),$email,$new_nom,$new_prenom,$equipement );
				if($reponse->Succes){
					$num_conn=Tools::getsession("num_conn");
					$this->database->execCustom("update 3g_inscrits set nom='$new_nom',prenoms='$new_prenom' where numero='$num_conn'");
					//$_SESSION["equipement"]=$new_equipement;
					$this->addMessage($this->MES_SUCESS,"Le changement de vos noms et prénoms se sont déroulés avec succès. MOOV CI vous remercie de votre confiance." );
					Tools::redirect($this->domain.$this->dashbord_url);
				}else{
					$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur)."." );
					Tools::redirect($this->domain.$this->change_password_url);

				}

			}else{

				$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur)."." );
				Tools::redirect($this->domain.$this->dashbord_url);



				//	$reponse=$client->RechargementCompte(Tools::getsession("id_conn"),'xxxxxxxxxxxxxx' );


				//	print_r($reponse);

			}
		}

	}


	//------------------------------
	public function change_email_processing(){
		$connected=$this->status_connexion();
	//	$reponse=new Moov3GResponseChangePassword();
	//	$client=new Moov3GSoapWebsiteClient();
		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);
		}else{
		//	echo "oooooo";
				$correct_email=$this->getEmail();
			$old_email=Tools::getPostValue("old_email");
			$new_email=Tools::getPostValue("new_email");
			$conf_email=Tools::getPostValue("conf_email");

			//-----------------------------------------------
		//	print_r($_POST);
		//	print_r($_SESSION);
             	$numero_conn=Tools::getsession("num_conn");
			$reponse=new Moov3GResponseModifieMembre();
			$client=new Moov3GSoapWebsiteClient();
		   $infos=$this->getInfosMembre();//on obtient les infos d elabonne via soap
		//	$equipement_soap=($infos["equipement"]=="") ? "SMARTPHONE" : $infos["equipement"];

			if(!$infos["success"]){
				$this->addMessage($this->MES_ERROR,$infos["error"] );
				Tools::redirect($this->domain.$this->change_password_url);
			}



			//--------------------------------
			if($_SESSION["n_email"]){// cas d' un ajout d email

				if((!filter_var($new_email,FILTER_VALIDATE_EMAIL))){
					$this->addMessage($this->MES_ERROR,"Votre nouvel email n' est pas un email valide.Veuillez le corriger.");

					Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);


				}else if($new_email!=$conf_email){
					$this->addMessage($this->MES_ERROR,"Vos 2 emails ne sont pas identiques.Veuillez les ressaisir.");
					Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);

				}else{



				    $reponse=$client->ModifieMembre(Tools::getsession("id_conn"),$new_email,$infos["nom"],$infos["prenom"],$infos["equipement"]);
					if($reponse->Succes){
						$this->database->execCustom("update 3g_inscrits set email='$new_email' where numero='$numero_conn'");

						$this->addMessage($this->MES_SUCESS,"Le changement de votre email s' est déroulé avec succès. MOOV CI vous remercie de votre confiance." );

						//	print_r($reponse);
						unset($_SESSION["n_email"]);

					//	echo "ok";
						Tools::redirect($this->domain.$this->dashbord_url);
					}else{
						$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur).".");
							Tools::redirect($this->domain.$this->change_password_url);
					}

				}

			}else{// cas d' un changement email normal


		 if((!filter_var($old_email,FILTER_VALIDATE_EMAIL))){

				$this->addMessage($this->MES_ERROR,"Votre ancien email n' est pas un email valide.Veuillez le corriger.");
		 	Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);

			}else if((!filter_var($new_email,FILTER_VALIDATE_EMAIL))){
				$this->addMessage($this->MES_ERROR,"Votre nouvel email n' est pas un email valide.Veuillez le corriger.");

				Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);


			}else if($old_email!=$infos["email"]){
				$this->addMessage($this->MES_ERROR,"l' ancien email ne correspond pas à celui que vous avez fourni à votre inscription.Veuillez le corriger.");
				Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);

			}else if($new_email!=$conf_email){
				$this->addMessage($this->MES_ERROR,"Vos 2 emails ne sont pas identiques.Veuillez les ressaisir.");
				Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);


			}else if(($old_email=="")||($new_email=="")||($conf_email=="")){

				$this->addMessage($this->MES_ERROR,"Un ou plusieurs champs sont vides.Veuillez remplir tous les champs avant de valider le formulaire.");
				Tools::redirect($this->domain.$this->change_password_url."?om=".$old_email."&n_m=".$new_email);


			}else{
				$reponse=$client->ModifieMembre(Tools::getsession("id_conn"),$new_email,$infos["nom"],$infos["prenom"],$infos["equipement"]);
				if($reponse->Succes){
					$this->database->execCustom("update 3g_inscrits set email='$new_email' where numero='$numero_conn'");

					$this->addMessage($this->MES_SUCESS,"Le changement de votre email s' est déroulé avec succès. MOOV CI vous remercie de votre confiance." );

					//	print_r($reponse);
				//	unset($_SESSION["n_email"]);
					Tools::redirect($this->domain.$this->dashbord_url);
				}else{
					$this->addMessage($this->MES_ERROR,utf8_encode($reponse->MessageErreur).".");
					Tools::redirect($this->domain.$this->change_password_url);
				}




				}
	    	}
		}
	}

	//-----------------------------------
	function recharge_compte(){

		$connected=$this->status_connexion();

		if(!$connected){
			//	echo "pppppp";
			Tools::redirect($this->domain.$this->login_url);
		}else{
			$reponse=new Moov3GResponseRechargementCompte();

			$client=new Moov3GSoapWebsiteClient();

			$reponse=$client->StatutAbonne(Tools::getsession("id_conn"));
	//		print_r($solde_obj);
			if(!$reponse->Succes){
				Tools::redirect($this->domain.$this->login_url);
			}else{
				//
				//	print_r($reponse);

				$this->vue->set_filenames(array("body"=>"operations/rechargement.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl"));
				$this->vue->assign_var("menu_underline_recharge","underline" );//on cache la div

				$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
				$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
				$this->vue->assign_var_from_handle("LOGIN","login" );
				$this->vue->assign_var_from_handle("MENU","menu" );
				$this->vue->assign_var_from_handle("HEADER","header");
				$this->vue->pparse("body");
			}

		}

	}
	//----------------------------------
	function change_password3(){

		$connected=$this->status_connexion();
        $old_mail=Tools::getQueryValue("om");
        $new_mail=Tools::getQueryValue("nm");
	//	print_r($_SESSION);
		if(!$connected){
			//	echo "pppppp";
			Tools::redirect($this->domain.$this->login_url);
		}else{

		//	print_r($this->getInfosMembre());
			$reponse=new Moov3GResponseChangePassword();

			$client=new Moov3GSoapWebsiteClient();

			$reponse=$client->StatutAbonne(Tools::getsession("id_conn"));
			//		print_r($solde_obj);
			if(!$reponse->Succes){
				Tools::redirect($this->domain.$this->login_url);
			}else{
				//
				//	print_r($reponse);
				if($_SESSION["n_email"]){
					if((empty($_GET))&&(!isset($_SESSION["msgcontent"]))){
						$this->addMessage($this->MES_WARNING,"Veuillez renseigner votre email.MOOV CI vous enverra des informations via cet email." );

					}

					$this->vue->assign_var("hide_old_email","hide" );
					$this->vue->assign_var("hide_change_password","hide" );
					//unset($_SESSION["n_mail"]);
				}

				$this->vue->set_filenames(array("body"=>"operations/change_password.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl"));

				$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
				$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
				$this->vue->assign_var("old_email",Tools::escapeVar($old_mail) );
				$this->vue->assign_var("new_email",Tools::escapeVar($new_mail) );
				$this->vue->assign_var("menu_underline_infos","underline" );//on cache la div

				$this->vue->assign_var_from_handle("LOGIN","login" );
				$this->vue->assign_var_from_handle("MENU","menu" );
				$this->vue->assign_var_from_handle("HEADER","header");
				$this->vue->pparse("body");
			}

		}

	}

	//-------------------------------
	function change_password(){

		$connected=$this->status_connexion();
		$old_mail=Tools::getQueryValue("om");
		$new_mail=Tools::getQueryValue("nm");
		//	print_r($_SESSION);
		if(!$connected){
			//	echo "pppppp";
			Tools::redirect($this->domain.$this->login_url);
		}else{

			//	print_r($this->getInfosMembre());
			$reponse=new Moov3GResponseChangePassword();

			$client=new Moov3GSoapWebsiteClient();

			$reponse=$client->StatutAbonne(Tools::getsession("id_conn"));
			//		print_r($solde_obj);
			if(!$reponse->Succes){
				Tools::redirect($this->domain.$this->login_url);
			}else{
				//
				//	print_r($reponse);
				if($_SESSION["n_email"]){
					if((empty($_GET))&&(!isset($_SESSION["msgcontent"]))){
						$this->addMessage($this->MES_WARNING,"Veuillez renseigner votre email.MOOV CI vous enverra des informations via cet email." );

					}

					$this->vue->assign_var("hide_old_email","hide" );
					$this->vue->assign_var("hide_change_password","hide" );
					//unset($_SESSION["n_mail"]);
				}

				$this->vue->set_filenames(array("body"=>"operations/infos_perso.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl"));

				$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
				$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
				$this->vue->assign_var("old_email",Tools::escapeVar($old_mail) );
				$this->vue->assign_var("new_email",Tools::escapeVar($new_mail) );
				$this->vue->assign_var("old_name",$_SESSION["realname"] );
				$this->vue->assign_var("old_prenom",$_SESSION["realprenom"] );
				$this->vue->assign_var("menu_underline_infos","underline" );//on cache la div

				$this->vue->assign_var_from_handle("LOGIN","login" );
				$this->vue->assign_var_from_handle("MENU","menu" );
				$this->vue->assign_var_from_handle("HEADER","header");
				$this->vue->pparse("body");
			}

		}

	}

	//----------------------------
	public function add_forfait(){
		$connected=$this->status_connexion();

		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);

		}else{
			$reponse=new Moov3GResponseSouscritAbonne();
			$client=new Moov3GSoapWebsiteClient();
			$reference_forfait=Tools::getQueryValue("ref");
			$reponse=$client->SouscritAbonne(Tools::getsession("id_conn"),$reference_forfait,"",true );

			if($reponse->Succes){
				$this->addMessage($this->MES_SUCESS,"Opération réussie.Votre nouveau forfait est actif.");
			}else{
				$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");

			}
			Tools::redirect($this->domain.$this->dashbord_url);

		}

	}
	//--------------------------------
		public function sendMessageToMoov(){

			$connected=$this->status_connexion();
			$line_separator="<br><br>";
			if(!$connected){
				Tools::redirect($this->domain.$this->login_url);
			}else{

				$error_list=array();
				$nom=Tools::getPostValue("nom");
				$prenom=Tools::getPostValue("prenom");
				$email=Tools::getPostValue("email");
				$sujet=Tools::getPostValue("sujet");
				$message=$_POST["message"];


				if(!Tools::is_alphabetic($nom)){
					$error_list[]="Veuillez saisir un nom valide";
				}

				if(!Tools::is_alphabetic($prenom)){
					$error_list[]="Veuillez saisir un prénom valide";
				}

				if(!Tools::is_alphanumeric($sujet)){
					$error_list[]="Veuillez saisir un sujet valide";
				}

				if(!Tools::is_email($email)){
					$error_list[]="Veuillez saisir un email valide";
				}

				if($message==""){
						$error_list[]="Votre message est vide ou contient des caractères invalides";
				}
			/*	if(!Tools::is_alphanumeric($message)){
					$error_list[]="Votre message est vide ou contient des caractères invalides";
				}*/

				if(!empty($error_list)){
					$this->addMessage($this->MES_ERROR,implode("<br>",$error_list ) );
					Tools::redirect($this->domain.$this->contact_url);
				}else{
					$mail_message.="NOM: ".$nom.$line_separator;
					$mail_message.="PRENOM: ".$prenom.$line_separator;
					$mail_message.="EMAIL: ".$email.$line_separator;
					$mail_message.="MESSAGE: ".str_replace("\n","<br>" ,$message ).$line_separator;

				//	$this->launchmail("interactiveneo@gmail.com",$mail_message );
					$this->launchmail($this->contact_clientele_moov,$mail_message );
					$this->addMessage($this->MES_SUCESS,"Votre message a été envoyée au service clientèle de MOOV CI.<br>Vous recevrez une réponse dans un délai de 48h" );
					Tools::redirect($this->domain.$this->dashbord_url);
				}
			}

		}



	//-------------------------
	public function contact_clientele(){

		$connected=$this->status_connexion();

		if(!$connected){
			//	echo "pppppp";
			Tools::redirect($this->domain.$this->login_url);
		}else{
			$reponse=new Moov3GResponseStatutAbonne();

			$client=new Moov3GSoapWebsiteClient();

			$reponse=$client->StatutAbonne(Tools::getsession("id_conn"));
			//		print_r($solde_obj);
			if(!$reponse->Succes){
				Tools::redirect($this->domain.$this->login_url);
			}else{
				//
				//	print_r($reponse);

				$this->vue->set_filenames(array("body"=>"operations/contact.tpl","header"=>"common/header.tpl","login"=>"common/login.tpl","menu"=>"common/menu.tpl"));
				$this->vue->assign_var("menu_underline_contact","underline" );//on cache la div

				$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
				$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
				$this->vue->assign_var_from_handle("LOGIN","login" );
				$this->vue->assign_var_from_handle("MENU","menu" );
				$this->vue->assign_var_from_handle("HEADER","header");
				$this->vue->pparse("body");
			}

		}




		}

	//--------------------------
	/**
	 *
	 *
	 */
	public function send_forfait(){
		$connected=$this->status_connexion();

		if(!$connected){
			Tools::redirect($this->domain.$this->login_url);

		}else{
			$statut=new Moov3GResponseStatutAbonne();
			$reponse=new Moov3GResponseSouscritAbonne();
			$client=new Moov3GSoapWebsiteClient();
			$reference_forfait=$_POST["ref"];
			$reference_forfait=trim($reference_forfait);
			$dest_forfait=Tools::getPostValue("dest_numero");
			$nom_forfait=Tools::getPostValue("nom_forfait");
			$password=Tools::getPostValue("password_ab");
			$correct_password=Tools::getsession("password_conn");
		  //  $correct_email=$this->getEmail();

         // print_r($_POST);
		//	echo "iii".$reference_forfait;
		//	echo "jjj".$dest_forfait;
			if(Tools::getsession("num_conn")==$dest_forfait){
				$this->addMessage($this->MES_ERROR,"Le numéro de destination et le numéro d' envoi sont identiques. Veuillez selectionner un autre numéro." );
					Tools::redirect($this->domain.$this->send_forfait_url);

			}else if($password!=$correct_password){
				$this->addMessage($this->MES_ERROR,"Le password que vous avez saisi est incorrecte." );
				Tools::redirect($this->domain.$this->send_forfait_url);
			}else{

				$reponse=$client->SouscritAbonne(Tools::getsession("id_conn"),$reference_forfait,$dest_forfait,true );

			/*	if(Tools::getsession("num_conn")=="42121327"){
				print_r($_POST);
					echo $_POST["ref"]."<br>";
					echo $reference_forfait."<br>";
					echo $dest_forfait."<br>";
					print_r($reponse);
				}*/
				if($reponse->Succes){
					$this->addMessage($this->MES_SUCESS,"Vous venez de transférer le forfait ".strtoupper($nom_forfait)." au numéro +225".$dest_forfait.".Le destinataire recevra un sms de confirmation.");
				}else{
					$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");

				}
			//	print_r($reponse);
			//	if(Tools::getsession("num_conn")!="42121327"){
					Tools::redirect($this->domain.$this->dashbord_url);

			//	}


			}
		//	$email_user=""

				}

	}

}

?>