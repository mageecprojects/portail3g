<?php

/**
 *
 *
 * @version $Id$
 * @copyright 2012
 */

class Tools{

	public static function redirect($value){
		header("Location:$value");
		exit();
	}
	public static function keyExist($value){
		if((isset($_SESSION[$value]))||(isset($_COOKIE[$value]))){
			return true;
		}else{
			return false;
		}
	}
	public static function addsession($key,$value){
		$_SESSION[$key]=$value;
	}

	public static function removeAccent($value){
		$string_without_accent = strtr( $value, 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY' );
		return $string_without_accent;
	}

	public static function getsession($key){
	return	$_SESSION[$key];
	}
	public static function getQueryValue($value){
		$result="";

		if(isset($_GET[$value])) $result=$_GET[$value];

	//	$result=htmlspecialchars($result);
		$result=mysql_real_escape_string($result);
		return $result;

	}

	public static function getPostValue($value){
		$result="";


		if(isset($_POST[$value])) $result=$_POST[$value];
	//	if(isset($_SESSION[$value])) $result=$_SESSION[$value];
		//$result=htmlspecialchars($result);
		$result=mysql_real_escape_string($result);
		return $result;

	}
	public static function getname($value){
		$ext=strrchr($value,"." );
	//	$getexist=file_exists("images/thumb/".$value);
		$nb=strlen($ext)*(-1);
		//	echo $nb;
		//	strr
		$originalname=substr($value,0,$nb );
		return $originalname;
	}


	public static function removeSession($value){
		unset($_SESSION[$value]);

	}
	public static function formatenglish($value){
		return number_format($value,2,'.','');
	}

	public  static function trimedtext($value,$maxsize){

		if(strlen($value)>$maxsize){
			return substr($value,0,$maxsize-3 )."...";
		}else{
			return $value;
		}
	}
	public static function formatenglishprice($value){
			return number_format($value);

	}

	public static function formatfrechprice($value,$precision=0){
		return number_format($value, $precision, ',', ' ');


	}


	public static function protectVar($value,$killaccent=false){

	$value=	mysql_real_escape_string($value);
	if($killaccent) $value=Tools::removeAccent($value);
		return $value;
	}
	//-------------------------------------
	public static function escapeVar($value){
		return htmlentities($value);
	}



	//---------------------------------


	//------------------------------
	public static function is_alphabetic($value){
		$alpha_pattern="/^[A-Za-z:éè\-_ ]+$/";
		return preg_match($alpha_pattern,$value );
	}
	//---------------------------------
	public static function is_alphanumeric($value){
		$pattern="/^[A-Za-z0-9:\-_ ]+$/";
		return preg_match($pattern,$value );
	}
	//--------------------------
	public static function is_email($value){
		$pattern="/^[A-Za-z0-9:.\-_]+@[A-Za-z0-9:.\-_]+\.[a-zA-Z]{2,}$/";;
		return preg_match($pattern,$value );
	}


	//---------------------------

	public static function acceptString($value){

		$value=str_replace("é","@" ,$value );
		$value=str_replace("è","*" ,$value );
	$result=preg_replace( "/[^A-Za-z\*@_:.\- ]/", "", $value );
		$result=str_replace("@","é" ,$result );
		$result=str_replace("*","è" ,$result );
		return $result;
	}

	public static function acceptStringS($value){
		return	preg_replace( "/[^A-Za-zéè_:.\- ]/", "", $value );
	}

	public static function acceptInteger($value,$defautl=0){
		$result=preg_replace( "/[^0-9]/", "", $value );
		if($result==""){
			$result=$defautl;
		}
		return $result	;
	}

	public static function acceptAlphanumeric($value,$defautl=0){
		$result=preg_replace( "/[^A-Za-z0-9éè_:\-]/", "", $value );
		if($result==""){
			$result=$defautl;
		}
		return $result	;
	}

	public static function replaceGetParameter($key,$value){
		$uri=$_SERVER['PHP_SELF'];
	//	$url=$_SERVER['REQUEST_URI'];
		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p,$key."=" );
		//	echo $p;
		//	echo $value;
		if($b===false){
			$prefix="";
			if($p!="") $prefix="&";
			$p.=$prefix.$key."=".$value;
			//	echo "ll";
		}else{
			$p=str_replace($key."=".$_GET[$key],$key."=".$value ,$p );
			//	echo "rep";
		}

		return $uri."?".$p;

	}

	//-----------------------------------------------------
	public static function convert_date_to_french($value){
	list($date,$heure)=explode(" ",$value);
	list($year,$month,$day)=explode("-",$date );
		return $day.'/'.$month."/".$year." à ".$heure;
	}
	//---------------------------------------------------
	public static function removeGetParameter($key,$uri){
	list($part1,$p)=explode("?",$uri );
		$c=strpos($p,"&".$key."=" );
		//	echo $p;
		if($c===false){
			$param=$key."=".$_GET[$key];
		}else{
			$param="&".$key."=".$_GET[$key];
		}
		//	echo $value;

	    $p=str_replace($param,"",$p);
		if($p!=""){
			$suf="?".$p;
		}else{
			 $suf=$p;
		}

		return $part1.$suf;

	}
	//------------------------------------------
	public static function getDay($time=""){
     if($time=="") $time=time();
		return date("d/m/Y ",$time);
	}

	//-----------------------------------
	public static function formatMoovNumber($value){
		$value=Tools::insertChar(3,$value);
		$value=Tools::insertChar(6,$value);
		$value=Tools::insertChar(9,$value);
		return $value;

	}
    //-----------------------------------------
	public static function insertChar($pos,$value,$char=" "){
		$w=substr($value,0,$pos-1 );
		$w2=substr($value,$pos-1,strlen($value)-$pos+1);


		return $w.$char.$w2;

	}
}
?>