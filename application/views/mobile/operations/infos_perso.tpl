{HEADER}
<div class="container_moov2">
<!--<div class="loading"></div> -->
  {LOGIN}
  <div class="menu_operation">

  {MENU}
  </div>

<div class="menu_infos">
   <div class="{ERROR_TYPE}">{ERROR_MESSAGE}</div>
    <!--  modifier email -->
 <h1 class="titleresumeCompte"> Modifier mon email</h1>

   <div class="tooltipeffect"></div>
  <div class="contentBloc">
    <div class="infos_cons">
     <form id="form1" name="form1" method="post" action="?ctrl=operations&action=change_email_processing">
      <div class="inputcontent {hide_old_email} ">
        <label for="code_recharge" class="verdana11 labeltrans">Ancien email :</label>
        <input type="text" name="old_email" id="old_email" class="inputtext"  value="{old_email}"/>
        </div>
         <div class="inputcontent ">
        <label for="code_recharge" class="verdana11 labeltrans">Nouvel email :</label>
        <input type="text" name="new_email" id="new_email" class="inputtext"  value="{new_email}"/>
        </div>
        
           <div class="inputcontent ">
        <label for="code_recharge" class="verdana11 labeltrans">Confirmez l' email :</label>
        <input type="text" name="conf_email" id="conf_email" class="inputtext" />
        </div>
        <div class="inputcontent">
    <input name="" type="submit"  class="submit_spons" style="margin-left:auto;margin-right:auto;margin-top:10px;" value="Envoyer"/>
    </div>
      </form>
    </div>
  </div>

<!--fin de modif email -->

   
   
   
   
   <div class="bloc_change_password {hide_change_password2}">
  <h1 class="titleresumeCompte"> Changer mon mot de passe</h1>
     
     <div class="tooltipeffect"></div>
     <div class="contentBloc">
       <div class="infos_cons">
         <form id="form1" name="form1" method="post" action="?ctrl=operations&action=change_password_processing">
           <div class="inputcontent ">
             <label for="code_recharge" class="verdana11 labeltrans">Ancien mot de passe :</label>
             <input type="password" name="old_password" id="old_password" class="inputtext" />
            </div>
           <div class="inputcontent ">
             <label for="code_recharge" class="verdana11 labeltrans">Nouveau mot de passe :</label>
             <input type="password" name="new_password" id="new_password" class="inputtext" />
            </div>
           
           <div class="inputcontent ">
             <label for="code_recharge" class="verdana11 labeltrans">Confirmez le mot de passe :</label>
             <input type="password" name="conf_password" id="conf_password" class="inputtext" />
            </div>
           <div class="inputcontent">
             <input name="" type="submit"  class="submit_spons" style="margin-left:auto;margin-right:auto;;margin-top:10px;" value="Envoyer"/>
            </div>
          </form>
        </div>
     </div>
   </div>
  
<h1 class="titleresumeCompte"> Modifier mon equipement</h1>

   <div class="tooltipeffect"></div>
  <div class="contentBloc">
    <div class="infos_cons">
     <form id="form1" name="form1" method="post" action="?ctrl=operations&action=change_device_processing">
      
         <div class="inputcontent ">
        <label for="code_recharge" class="verdana11 labeltrans">Selectionner l'  equipement :</label>
      <select name="new_equipement" class="inputselect" id="new_equipement">
         <option value="SMARTPHONE">Smartphones</option>
         <option value="CLE">Clé internet</option>
         <option value="TABLETTE">Tablette </option>
         <option value="BOX">Box Wifi</option>
       </select>
        </div>
        
        
        <div class="inputcontent">
    <input name="" type="submit"  class="submit_spons" style="margin-left:auto;margin-right:auto;;margin-top:10px;" value="Envoyer"/>
    </div>
      </form>
    </div>
  </div>
 <!--modifier nom et prénoms
--> 
<h1 class="titleresumeCompte"> Modifier le nom et le prénom</h1>

   <div class="tooltipeffect"></div>
  <div class="contentBloc">
    <div class="infos_cons">
     <form id="form1" name="form1" method="post" action="?ctrl=operations&action=change_name_processing">
      <div class="confirm_send" style="display:block;">le nom et le prénom ne doivent comporter que des lettres alphabétiques sans aucun accent</div>
       <div class="inputcontent ">
            <label for="code_recharge" class="verdana11 labeltrans">Nom:</label>
             <input type="text" name="nom_user" id="nom_user" class="inputtext"  value="{old_name}" />
            </div>
            
          <div class="inputcontent ">
            <label for="code_recharge" class="verdana11 labeltrans">Prénoms :</label>
            <input type="text" name="prenom_user" id="prenom_user" class="inputtext"  value="{old_prenom}" />
            </div>
        
        
        <div class="inputcontent">
    <input name="" type="submit"  class="submit_spons" style="margin-left:auto;margin-right:auto;;margin-top:10px;" value="Envoyer"/>
    </div>
      </form>
    </div>
  </div>
 





 
  </div> <!--fin de menu infos -->
  
  
</div> <!--fin de container moov -->
</body>
</html>
