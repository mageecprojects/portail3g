<?php

require_once("baseController.php");

class inscriptionController extends baseController{
	/**
	 * Page-level DocBlock
	 *
	 *
	 * @todo ajouter le slangues a droite
	 *
	 * @todo faire le design d ela page d accueil
	 * @todo faire le routage correcte des pages
	 *
	 *
	 *
	 *
	 */
   public function index(){

  echo "ok";
   }

	//-------------------------------
	public function inscription(){

		$connected=$this->status_connexion();

		if(!$connected){

			if(!$this->isMobileDevice()) $this->addJs(array("controlins.js"));

			$this->vue->set_filenames(array("body"=>"inscription/inscription.tpl","header"=>"common/header.tpl"));
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
			$this->vue->pparse("body");
		}else{
			Tools::redirect($this->domain.$this->dashbord_url);
		}





	}
	//------------------------
	public function forgetpass(){
		$this->vue->set_filenames(array("body"=>"inscription/forget_pass.tpl","header"=>"common/header.tpl"));
		$this->vue->assign_var_from_handle("HEADER","header");
		$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
		$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
		$this->vue->pparse("body");



	}

	//--------------------------
	public function inscription_processing(){

		$numero=Tools::getPostValue("numero_ab");
		$numero2=Tools::getPostValue("numero2_ab");
		$email=Tools::getPostValue("email_ab");
		$nom=Tools::getPostValue("nom_ab");
		$prenom=Tools::getPostValue("prenom_ab");
		$reponse=new Moov3GResponseInscriptionMembre();
		$equipement=Tools::getPostValue("equipement");
		$equipement_soap=$this->device_list_soap[$equipement];
		$nom=strtoupper($nom);
		$prenom=strtoupper($prenom);

	//	print_r($_SESSION);
	//	print_r($_POST);
	//	$insert_inscrit2="insert into 3g_inscrits2 values (null,'$numero','$numero2') ";
	//	$this->database->execCustom($insert_inscrit2);
        $numequipement_pattern="/^(01|02|03|40|41|42)[0-9]{6}$/i";
        $nummobile_pattern="/^[0-9]+$/i";
        $alphapattern="/^[A-Z\- ]+$/i";

		//$this->correct_captcha();
		if(!$this->correct_captcha()){

		$this->addMessage($this->MES_ERROR,"Le code de sécurité est incorrecte.");
		//	echo "incorrecte";

		}elseif (!preg_match($numequipement_pattern,$numero )){
				$this->addMessage($this->MES_ERROR,"Le numéro fourni n' est pas un numéro moov.");

		 }elseif (!preg_match($nummobile_pattern,$numero2 )){
		  $this->addMessage($this->MES_ERROR,"Le numéro mobile est invalide.");

		 }elseif (!preg_match($alphapattern,$nom )){
	       $this->addMessage($this->MES_ERROR,"Le nom doit etre en majuscule sans accent.");

		 }elseif (!preg_match($alphapattern,$prenom )){
		 	$this->addMessage($this->MES_ERROR,"Le prénom doit etre en majuscule sans accent.");

		 }elseif (!$this->validEmail($email)) {
			$this->addMessage($this->MES_ERROR,"Le format de votre email est invalide.");

		}else{
			$soap_client=new Moov3GSoapWebsiteClient();
			$reponse=$soap_client->InscriptionMembre($numero,$email,$nom,$prenom,$equipement_soap);


			if($reponse->Succes){
				$password=$reponse->Password;
				$this->remove_inscrit($numero);
				$insert_inscrit="insert into 3g_inscrits values (null,'$numero','$password','$email','$nom','$prenom','$equipement_soap','$numero2') ";
				$insert_inscrit2="insert into 3g_inscrits2 values (null,'$numero','$numero2') ";
				$this->database->execCustom($insert_inscrit);
				$this->database->execCustom($insert_inscrit2);
				$this->addMessage($this->MES_SUCESS,"Merci de votre inscription à MOOV3G. Votre mot de passe vous sera envoyé par SMS." );
			}else{
				$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");

			}
		}
          //   print_r($reponse);
		Tools::redirect($this->domain.$this->inscription_url);
	    //var_dump($reponse);


	}

	//----------------------
	public function reinit_processing(){

		$numero=Tools::getPostValue("numero_ab");
		$email=Tools::getPostValue("email_ab");

		//	print_r($_SESSION);
		//	print_r($_POST);

		//$this->correct_captcha();
		$reponse=new Moov3GResponseReinitialisePassword();
		if(!$this->correct_captcha()){

			$this->addMessage($this->MES_ERROR,"Le code de sécurité est incorrecte.");
			//	echo "incorrecte";

		}else{
			$soap_client=new Moov3GSoapWebsiteClient();
			$reponse=$soap_client->ReinitialisePassword($numero,$email);


			if($reponse->Succes){


			//	$this->database->execCustom("delete from 3g_inscrits where numero='$numero'");
		        $new_password=$reponse->Password;
				$update_ins="update 3g_inscrits set pass_word ='$new_password' where numero='$numero'";
				$this->database->execCustom($update_ins);
				$this->addMessage($this->MES_SUCESS,"Votre mot de passe a été reinitialisée avec succès.Un sms de confirmation vous sera envoyée." );
			}else{
				$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");

			}
		}

		Tools::redirect($this->domain.$this->forget_url);
		//var_dump($reponse);


	}

	//--------------------

	public function connexion_processing(){

		$numero=Tools::getPostValue("numero_ab");
		$password=Tools::getPostValue("password_ab");
	//	$equipement=Tools::getPostValue("equipement");
		$current_date=date("Y-m-d H:i:s");

         $nom_soap=$prenom_soap=$equipement=$mail_soap="";
		//	print_r($_SESSION);

		//$this->correct_captcha();
		if(!$this->correct_captcha()){

			$this->addMessage($this->MES_ERROR,"Le code de sécurité est incorrecte.");
			//	echo "incorrecte";
			Tools::redirect($this->domain.$this->login_url);


		}else{
			$soap_client=new Moov3GSoapWebsiteClient();
            $reponse_infos_abonne=new Moov3GResponseInfoMembre();
			$reponse=$soap_client->ConnexionMembre($numero,$password);
          //  print_r($reponse);

			if($reponse->Succes){
			//	echo "success".$reponse->IdConnexion;
				//-------------------------------------------
				$infos=$this->getInfosMembre($reponse->IdConnexion);
             //   print_r($infos);
				$this->openSession($numero,$reponse->IdConnexion,$infos["equipement"],$password );
				$this->saveLastConnectionDate();
				$this->database->execCustom("delete from 3g_connexion where numero='$numero'");

				$insert_conn="insert into 3g_connexion values (null,'$numero','$current_date') ";
				$this->database->execCustom($insert_conn);

				//	$this->addMessage($this->MES_SUCESS,".Merci de votre inscription à MOOV3G.Votre mot de passe vous sera envoyé par SMS" );
				//-----------------
				//	$email_user=$this->getEmail();
				//echo "user:".$email_user."<br>";
				if($infos["email"]==""){//pas d email chez moov

					$this->remove_inscrit($numero);
					$this->add_inscrit($numero,$password ,$infos["email"],$infos["nom"],$infos["prenom"],$infos["equipement"]);
					// si il  a un mail chez moov on le redirige vers le changement d email
					$_SESSION["n_email"]=true;
				//	echo "ppp";
						Tools::redirect($this->domain.$this->change_password_url);


				}else{
			//	echo "pppiii";
					Tools::redirect($this->domain.$_SESSION["previous"]);

				}

				//---------------

				}else{
					$this->addMessage($this->MES_ERROR, utf8_encode($reponse->MessageErreur).".");
					Tools::redirect($this->domain.$this->login_url);
				}
				//-------------------------------

			}
		}


		//var_dump($reponse);







	//-------------------------------
	public function connexion(){
		$connected=$this->status_connexion();
		if(!$connected){
			$this->vue->set_filenames(array("body"=>"inscription/connexion.tpl","header"=>"common/header.tpl"));
			$this->vue->assign_var_from_handle("HEADER","header");
			$this->vue->assign_var("ERROR_TYPE",$this->getMessagetype() );
			$this->vue->assign_var("ERROR_MESSAGE",$this->getMessageContent() );
			$this->vue->pparse("body");
		}else{
			Tools::redirect($this->domain.$this->dashbord_url);
		}



		/*
		   $response = $clt->InscriptionMembre('03084182', 'lebdenat@hotmail.com') ;
		   if(! $response->Succes)
		   {
		   echo $response->MessageErreur ;
		   }
		   else
		   {
		   echo "Inscription reussie" ;
		   }
		*/


	}


	//--------------------

}

?>