<?php
	
	if(! defined('MOOV3G_SOAP_CLIENT_INCLUDED'))
	{
	
		define('MOOV3G_SOAP_CLIENT_INCLUDED', 1) ;
		if(! defined("ASMX_CLIENT_INCLUDED"))
		{
			include dirname(__FILE__)."/AsmxClient.class.php" ;
		}
		
		class Moov3GSoapClient extends AsmxClient
		{
			// var $Url = "http://10.177.18.39:99/Service3.svc?wsdl" ;
			var $Url = "http://localhost:21931/HTTPGateWays/Service3.svc?wsdl" ;
			public $SoapActionEnabled = 1 ;
			public $InterfaceName = "IService3" ;
			public $ParamNamePrefix = "" ;
			public $MethodNamePrefix = "tem" ;
			public $Xmlns = array("moov" => "http://schemas.datacontract.org/2004/07/Moov3G.Old.Message") ;
			function InscriptionMembre($msisdn, $email)
			{
				$node = $this->CallMethod(
					"InscriptionMembre",
					$this->RequestData(
						array('moov:Msisdn' => $msisdn, 'moov:Email' => $email)
					)
				) ;
				return new Moov3GReponseInscriptionMembre($node) ;
			}
			function ConnexionMembre($msisdn, $password)
			{
				$node = $this->CallMethod(
					"ConnexionMembre",
					$this->RequestData(
						array('moov:Msisdn' => $msisdn, 'moov:Password' => $password)
					)
				) ;
				return new Moov3GReponseConnexionMembre($node) ;
			}
			function ExtractMethodXmlResponse($methodName, & $responseNode)
			{
				if(empty($responseNode))
					return false ;
				if(
					! isset($responseNode[0]["child"])
					|| ! isset($responseNode[0]["child"][0])
					|| $responseNode[0]["child"][0]["name"] != "S:BODY"
					|| ! isset($responseNode[0]["child"][0]["child"])
					|| ! isset($responseNode[0]["child"][0]["child"][0])
					|| $responseNode[0]["child"][0]["child"][0]["name"] != strtoupper($methodName)."RESPONSE"
					|| ! isset($responseNode[0]["child"][0]["child"][0]["child"])
					|| ! isset($responseNode[0]["child"][0]["child"][0]["child"][0])
					|| $responseNode[0]["child"][0]["child"][0]["child"][0]["name"] != strtoupper($methodName)."RESULT"
				)
					return false ;
				$result = array() ;
				if(isset($responseNode[0]["child"][0]["child"][0]["child"][0]["child"]))
				{
					$result = $responseNode[0]["child"][0]["child"][0]["child"][0] ;
				}
				return $result ;
			}
			function RequestData($params=array())
			{
				$data["tem:request"] = array() ;
				foreach($params as $name => $value)
				{
					$data["tem:request"][$name] = $value ;
				}
				return $data ;
			}
		}
		
		class Moov3GResponseBase
		{
			public $AutoImportNode = true ;
			public $ImportableAttrs = array() ;
			public $MessageErreur = "" ;
			public $Succes = "" ;
			public $LastConfigNode = null ;
			public function __construct($nodeTemp)
			{
				if($this->AutoImportNode)
				{
					$this->ImportConfigFromNode($nodeTemp) ;
				}
			}
			function ImportConfigFromNode(&$configNode)
			{
				$this->LastConfigNode = & $configNode ;
				if(isset($configNode["attrs"]))
				{
					foreach($configNode["attrs"] as $name => $value)
					{
						if(in_array($name, $this->ImportableAttrs))
						{
							$attrNode = array(
								"name" => $name,
								"content" => $value,
								"child" => array()
							) ;
							$this->ImportConfigMemberFromNode($attrNode) ;
						}
						else
						{
							$this->ImportConfigMemberFromKey($name, $value) ;
						}
					}
				}
				if(isset($configNode["child"]))
				{
					for($i=0; $i<count($configNode["child"]); $i++)
					{
						$this->ImportConfigMemberFromNode($configNode["child"][$i]) ;
					}
				}
				$this->UpdateConfigAfterImport() ;
			}
			function ImportConfigMemberFromKey($name, $value)
			{
				return false ;
			}
			function ImportConfigMemberFromNode(&$node)
			{
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MESSAGEERREUR" :
					{
						$this->MessageErreur = $nodeContent ;
					}
					break ;
					case "A:SUCCES" :
					{
						$this->Succes = ($nodeContent == "true") ? 1 : 0 ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
			function UpdateConfigAfterImport()
			{
			}
		}
		class Moov3GResponseMembreConnecte extends Moov3GResponseBase
		{
			public $Msisdn ;
			public $IdConnexion ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MSISDN" :
					{
						$this->Msisdn = $nodeContent ;
					}
					break ;
					case "A:IDCONNEXION" :
					{
						$this->IdConnexion = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseMembreDeconnecte extends Moov3GResponseBase
		{
			public $Msisdn ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MSISDN" :
					{
						$this->Msisdn = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GReponseInscriptionMembre extends Moov3GResponseMembreDeconnecte
		{
			public $Password = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:PASSWORD" :
					{
						$this->Password = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GReponseConnexionMembre extends Moov3GResponseMembreDeconnecte
		{
			public $IdConnexion = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:IDCONNEXION" :
					{
						$this->IdConnexion = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
	}
	
?>