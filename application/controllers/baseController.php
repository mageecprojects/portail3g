<?php
/**
 * @todo -c Implement . remettre le https sur le domaine avant d emettre en production
 */
class baseController{
	/**
	 * Constuctor
	 * priv
	 */
	protected $database;
	protected $vue;
	protected $current_user;
	protected $generaltitle;
	protected $abregedtitle;
	protected $logstatus;
	protected $domain;
	protected $MES_SUCESS;
	protected $MES_ERROR;
	protected $MES_WARNING;
	protected $MES_INFOS;
	protected $previous_url;
	protected $login_url;
	protected $inscription_url;
	protected $logout_url;
	protected $forget_url;
	protected $offres_url;
	protected $dashbord_url;
	protected $device_list;
	protected $device_list_soap;
	protected $send_forfait_url;
	protected $stop_forfait_url;
	protected $change_password_url;
	protected $recharge_url;
	protected $contact_url;
	protected $pay_as_go_url;
	protected $contact_clientele_moov;
//	protected $contact_clientele_moov;





	function __construct(){

        //	session_start();
	//	print_r($_SESSION);
	$this->domain="http://".$_SERVER["HTTP_HOST"].dirname($_SERVER['PHP_SELF'])."/";
		//$this->domain="https://".$_SERVER["HTTP_HOST"].dirname($_SERVER['PHP_SELF'])."/";


     //  require_once('config.php');
		$this->contact_clientele_moov="selfcare3g@moov.com";
        $this->generaltitle="Moov Portail 3G|Achetez vos forfaits 3g en ligne et suivez votre consommation";
		$this->device_list=array(
	    "0"=>"SMARTPHONE",
	    "1"=>"CLE USB",
	    "2"=>"TABLETTE",
	    "3"=>"BOX"
	     );

		$this->device_list_soap=array(
		"0"=>"SMARTPHONE",
		"1"=>"CLE",
		"2"=>"TABLETTE",
		"3"=>"BOX"
	);
		$this->MES_SUCESS="success";
		$this->MES_ERROR="error";
		$this->MES_WARNING="warning";
		$this->MES_INFOS="info";
		$this->login_url="connexion/";
	//	$this->login_url="?ctrl=inscription&action=connexion";
	//	$this->inscription_url="?ctrl=inscription&action=inscription";
		$this->inscription_url="getpassword/";
		$this->pay_as_go_url="account/payg/";
		$this->logout_url="?ctrl=operations&action=logout";
		$this->forget_url="account/password/reinit/";
	//	$this->forget_url="?ctrl=inscription&action=forgetpass";
	//	$this->offres_url="?ctrl=operations&action=forfaits";
		$this->offres_url="account/packs/";
		$this->dashbord_url="account/";
		//$this->send_forfait_url="?ctrl=operations&action=offrir_forfaits";
		$this->send_forfait_url="account/send/";
		$this->stop_forfait_url="?ctrl=operations&action=stop";
	//	$this->change_password_url="?ctrl=operations&action=change_password";
		$this->change_password_url="account/password/change/";
		$this->recharge_url="recharge/";
		$this->contact_url="account/contact/";
	   require_once('config.php');


	$this->database=new PDOConfig('mysql',$DB_HOST,$DB_DATABASE,$DB_USER,$DB_PASS);
		$this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		mysql_connect($DB_HOST,$DB_USER,$DB_PASS);
		if($this->isMobileDevice()){
		//	echo "mobile";
			$this->vue=new Template('application/views/mobile');

		}else{
			$this->vue=new Template('application/views/pc/');

		}
		//	$this->vue=new Template('application/views/');
		$this->vue->assign_var("PAGE_TITLE",$this->generaltitle );
		$_SESSION["previous"]=$this->dashbord_url;
		$this->assignConnInfos();




	}



	//--------------------------------------
	function getEmail(){
		$numero=Tools::getsession("num_conn");
		$search="select email from 3g_inscrits where numero='$numero'";
		$reponse=$this->database->executeOne($search);
		if(empty($reponse)){
			return null;
		}else{

				return $reponse["email"];

		}
	//	return "ok";
	}


	//-------------------------------
	function remove_inscrit($numero){

	  $sql="delete from 3g_inscrits where numero='$numero'";
		$this->database->execCustom($sql);
	}


	//-------------------------------
	function add_inscrit($numero,$password,$email="",$nom="",$prenom="",$equipement=""){

	$insert_inscrit="insert into 3g_inscrits values (null,'$numero','$password','$email','$nom','$prenom','$equipement')";
		$this->database->execCustom($insert_inscrit);

	}

	//----------------------------
	public function filterForfaits($liste_offres,$equipement){

        $res=array();
		$list_prix=array();
		$offre=new Moov3GOffre();
		$liste_good_forfaits=array();
	/*	$liste_good_forfaits["0"]=array("jour","epiq","semaine","plus","extra");
		$liste_good_forfaits["2"]=array("premium","maxi","extra");
		$liste_good_forfaits["3"]=array("premium","maxi","extra");
		$liste_good_forfaits["1"]=array("maxi","semaine","plus","extra");*/

	/*	$liste_good_forfaits["0"]="/jour|epiq|semaine|plus|extra/i";
		$liste_good_forfaits["1"]="/maxi|semaine|plus|extra/i";
		$liste_good_forfaits["2"]="/premium|maxi|extra/i";
		$liste_good_forfaits["3"]="/premium|maxi|extra/i";*/

	//	$pattern=$liste_good_forfaits[$equipement];

		foreach($liste_offres as $offre){
			$nom_forfait=$offre->Nom;
		//	if(preg_match($pattern,$nom_forfait )){
				$res[]=$offre;
				$list_prix[]=intval($offre->Montant);
		//	}
		}
		//print_r($list_prix);
		array_multisort($list_prix, SORT_ASC,SORT_NUMERIC,$res);
          return $res;
	}

	//-------------------------------
	public function getInfosMembre($conn=null){
		$infos=new Moov3GResponseInfoMembre();
		$client=new Moov3GSoapWebsiteClient();
		if($conn==null){
			$conn=Tools::getsession("id_conn");
		}
		$infos=$client->InfoMembre($conn);

		if(Tools::getsession("num_conn")=="42689986"){
		//	var_dump($infos);
		}
		if($infos->Succes){
			$nom=($infos->Nom=="") ? "ABONNE":$infos->Nom;
			$prenoms=($infos->Prenoms=="")? "SANS NOM":$infos->Prenoms;
			if($nom=="ABONNE"){
				$fullname="";
			}else{
				$fullname=$nom." ".$prenoms;
			}
			$equipement_soap=($infos->Equipement=="")? "SMARTPHONE":$infos->Equipement;

			$_SESSION["realname"]=$infos->Nom;
			$_SESSION["realprenom"]=$infos->Prenoms;

			return array("success"=>true,"nom"=>$nom,"prenom"=>$prenoms,"equipement"=>$equipement_soap,"email"=>$infos->Email,"fullname"=>$fullname,"realnom"=>$infos->Nom,"realprenom"=>$infos->Prenoms);
		}else{
			return array("success"=>false,"error"=>utf8_encode($infos->MessageErreur).".");
		}
	}



	//-------------------------------
	//cette fonction permet d attribuer automatiquement dans la vue les liens de connexion,
	//deconnexion,infos du numero connecté a tous les endrois qui en ont besoin
	public function assignConnInfos(){
		$connected=$this->status_connexion();
		$equipement=Tools::getsession("equipement");
		if($connected) {
		$infos=$this->getInfosMembre();
		}else{
			$infos=array();
		}

			$this->vue->assign_var("log_num",Tools::getsession("num_conn"));
			$this->vue->assign_var("log_fullname",ucwords($infos["fullname"]));
			$this->vue->assign_var("GL_INSCRIPTION",$this->inscription_url);
			$this->vue->assign_var("GL_CONNEXION",$this->login_url);
			$this->vue->assign_var("GL_DECONNEXION",$this->logout_url);
			$this->vue->assign_var("GL_FORGETPASS",$this->forget_url);
			$this->vue->assign_var("GL_FORFAITS",$this->offres_url);
			$this->vue->assign_var("GL_DASHBOARD",$this->dashbord_url);
			$this->vue->assign_var("GL_SEND_FORFAIT",$this->send_forfait_url);
			$this->vue->assign_var("CONSO_RATIO","0");
		//	$this->vue->assign_var("device",$this->device_list[$equipement]);
			$this->vue->assign_var("device",$equipement);
			$this->vue->assign_var("GL_STOP_FORFAIT",$this->stop_forfait_url);
			$this->vue->assign_var("GL_CHANGE_PASS",$this->change_password_url);
			$this->vue->assign_var("DOMAIN",$this->domain);
			$this->vue->assign_var("GL_RECHARGE",$this->recharge_url);
			$this->vue->assign_var("user_nom",$infos["nom"]);
			$this->vue->assign_var("user_prenoms",$infos["prenom"]);
			$this->vue->assign_var("user_email",strtolower($infos["email"]));
			$this->vue->assign_var("GL_RECHARGE",$this->recharge_url);
			$this->vue->assign_var("GL_CONTACT",$this->contact_url);
			$this->vue->assign_var("GL_PAYG",$this->pay_as_go_url);
	}

	//------------------------
	public function isMobileDevice(){


		$type_device=new Mobile_Detect();
		if(($type_device->isMobile())||($type_device->isTablet())){
			//	echo "mobile";
			return true;
		}else{
		//	echo "pc";
			return false;
		}


	}


	//----------------------------------
	public function saveLastConnectionDate($debug=0){
		$current_numero=Tools::getsession("num_conn");
		$sql_conn="select last_connexion from 3g_connexion where numero='$current_numero'";
	    $rep=$this->database->executeOne($sql_conn,$debug);
		$res= Tools::convert_date_to_french($rep["last_connexion"]);
		$_SESSION["last_connexion_date"]=$res;
		return $res;
	}


	//-------------------------
	public function getLastConnectionDate($debug=0){

		return Tools::getsession("last_connexion_date");
	}


	//------------------------------
	public function ConvertVolumeOffre($value){
		$unit="Mo";
		$res=$value;
		if($value>=1024){
			$res=$value/1024;
			$unit="Go";
		}
	return $res." ".$unit;

	}

	//-----------------------------------
	public function ConvertVolumeWithoutUnit($value){
		$res=$value/(1024*1024);
		return round($res,2);


	}
	//------------------------
	public function ConvertConsoRatio($maxvalue,$conso_rest){


		return floor((($conso_rest/$maxvalue)*100));

	}

	//-----------------------------


	//--------------------------
	public function ConvertVolume($value){
		$unit="Mo";
		$nb_mo=($value/(1024*1024));
		$result=$nb_mo;
		$octet_rest=($value%(1024*1024));
		if($octet_rest==0){
			//on teste la valeur en giga
				$reste_giga=($nb_mo%1024);
			if($reste_giga==0){
				$result=$nb_mo/1024;
				$unit="Go";
			}

		}

	  $result=round($result,2);

		if($result==0){
			$unit="Mo";
		}

		return Tools::formatfrechprice($result)." ".$unit;
	}

	//-------------------------
	public function setRedirectUrl(){
		$_SESSION["previous"]=$_SERVER["REQUEST_URI"];
	}

	//------------------------------------
	public function correct_captcha(){

		$securimage = new Securimage();
	//	echo "cap".$securimage->check($_POST['captcha_code']);
		if ($securimage->check($_POST['captcha_code']) == false) {

		//	echo "code incorrecte";
			return false;
		}else{
			return true;

		//	echo "code _correcte";
		}

	}

	//-------------------------------------
	public function addMessage($type,$text,$prefix="msg"){

		$_SESSION[$prefix."type_error"]=$type;
		$_SESSION[$prefix."content"]=$text;

	}

	//------------------------------------
    public function getMessagetype($prefix="msg"){
		$res=$_SESSION[$prefix."type_error"];
		unset($_SESSION[$prefix."type_error"]);
		return $res;
	}

	public function getMessageContent($prefix="msg"){

		$res=$_SESSION[$prefix."content"];
		unset($_SESSION[$prefix."content"]);
		return $res;

	}
	//-------------------------------
    function generePasswordStrong($nbcaracters=8){
    	$lettres="abcdefghijklmnopqrstpuvwxyz";
    	$chiffres="0123456789";
    	$result="";
    	for($i=0;$i<$nbcaracters;$i++){

    	$char_list=	(mt_rand(0,1))? $lettres:$chiffres ;
    	$index=mt_rand(0,intval(($char_list)-1));
    		$char=$char_list[$index];
    		$result.=$char;
    	}

    	return $result;
    }

	//------------------------------------

	//------------------------------------
	public function connect($user,$pass){


	}
	public function logoutnoreturn(){

	}
	public function status_connexion($unlogged="false"){

		if(!isset($_SESSION["islogged"])){
			return FALSE;
		}else{
			return true;
		}

		}


	//-----------------------------------------------
	function getUserInfos($key){


	}


	//-------------------------------------
	public function openSession($numero,$id_connexion,$equipement,$password){
		$_SESSION["islogged"]=true;
		$_SESSION["num_conn"]=$numero;
		$_SESSION["id_conn"]=$id_connexion;
		$_SESSION["equipement"]=$equipement;
		$_SESSION["password_conn"]=$password;
	}

	//--------------------------------------------
	function validEmail($value){
		return filter_var($value,FILTER_VALIDATE_EMAIL);



	}


	//----------------------------------------
	public function launchmail($dest,$mes,$sub="MOOV PORTAIL 3G",$nexp="MOOV PORTAIL 3G"){

		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
		//  echo "over";
		$mail->isMail(); // telling the class to use SMTP
		//	$mail->IsSMTP();

try {
	//	$mail->Host       = "mail.yourdomain.com"; // SMTP server
	//	$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
	//	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	//	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
	//	$mail->Host       = "ssl://smtp.gmail.com";      // sets GMAIL as the SMTP server
//	$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
//	$mail->Username   = "interactiveneo@gmail.com";  // GMAIL username
//	$mail->Password   = "neographics";          // GMAIL password
	//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)*/
	        // GMAIL password
//	$mail->AddReplyTo('noreply@akasicars.com', '');
	$mail->AddAddress($dest);
	//$mail->AddAddress("interactiveneo@gmail.com");

    $mail->SetFrom('portail3g@moov.com', $nexp);
		$mail->AddReplyTo('rce@moov.com', '');
	$mail->Subject = $sub;
	$mail->CharSet = 'UTF-8';
	$mail->AltBody = ''; // optional - MsgHTML will create an alternate automatically
	$mail->MsgHTML($mes);
	//	$mail->AddAttachment('images/phpmailer.gif');      // attachment
	//$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
	$mail->Send();
	$mail->ClearAddresses();
	$mail->AddAddress("interactiveneo@gmail.com");
	$mail->Send();
	//	echo "Message Sent OK</p>\n";
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
	echo $e->getMessage(); //Boring error messages from anything else!
}
	}

	//--------------------------------------------

	//-------------------------------------------------

	//----------------------------
	public function cryptdata($value){
		$alphabet="abcdefghiljkmnopqrstuvwxyz";
		$leure=$alphabet[mt_rand(0,25)];
		$value=base64_encode($value);
		$value=substr($value,0,2).$leure.substr($value,2);
		return $value;
	}

	//--------------------------------------------
	public function decrypt($value){
		$value=substr($value,0,2).substr($value,3);
		$value=base64_decode($value);
		return $value;
	}
	//----------------------------------
	//-------------------------------------------
	//---------------------------------------
	//-------------------------------------


	//------------------------------------------


	//----------------------------------------



	//-----------------------------------------
	//----------------------------------



	//-----------------------------------

	//------------------------------------

	//----------------------------------

	//---------------------------------
	//--------------------------------
	public function generateRandomcode(){
		$prefix=date('Y').date('m').date('d').date('H').date('i').date('s');
		$idunique=uniqid($prefix);
		return $idunique;
	}


	//----------------------------

	//----------------------------
	function checkbrowser(){
		$res=$this->browser_info();

		if(array_key_exists("msie",$res )){
			$version=intval($res["msie"]);
			if($version<7){
				$this->error_duel("Votre navigateur a de nombreuses failles de sécurité et des fonctionnalités manquantes,il n' est donc pas compatible avec cette opération,Veuillez le mettre à jour",true);

			}
		}
	}

	//---------------------------------
	function browser_info($agent=null) {
		// Declare known browsers to look for
		$known = array('msie', 'firefox', 'safari', 'webkit', 'opera', 'netscape',
		  'konqueror', 'gecko');

		// Clean up agent and build regex that matches phrases for known browsers
		// (e.g. "Firefox/2.0" or "MSIE 6.0" (This only matches the major and minor
		// version numbers.  E.g. "2.0.0.6" is parsed as simply "2.0"
		$agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);
		$pattern = '#(?<browser>' . join('|', $known) .
		  ')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';

		// Find all phrases (or return empty array if none found)
		if (!preg_match_all($pattern, $agent, $matches)) return array();

		// Since some UAs have more than one phrase (e.g Firefox has a Gecko phrase,
		// Opera 7,8 have a MSIE phrase), use the last one found (the right-most one
		// in the UA).  That's usually the most correct.
		$i = count($matches['browser'])-1;
		return array($matches['browser'][$i] => $matches['version'][$i]);
	}

	//-------------------------------------------------


	//--------------------------
	public function getpaginglink($value){
		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p ,"&Page=");
		if($b===false){
			$p.="&Page=".$value;
		}else{
			$p=str_replace("&Page=".$this->getvalue("Page"),"&Page=".$value ,$p );
		}

		return "search_car_result.do?".$p;
	}

	//----------------------------
	public function getpaginglink2($value){
		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p,"&Page=" );
		//	echo "paging";
		if($b===false){
			//	echo "concat";
			$p.="&Page=".$value;
		}else{
			//		echo " replace";
			$p=str_replace("&Page=".$this->getvalue("Page"),"&Page=".$value ,$p );
		}

		return "search_global.do?".$p;
	}
	//---------------------------
	public function getPaginationLink($value,$key,$uri){

		$p=$_SERVER["QUERY_STRING"];
		$b=strpos($p,$key."=" );
		//	echo $p;
		//	echo $value;
		if($b===false){
			$prefix="";
			if($p!="") $prefix="&";
			$p.=$prefix.$key."=".$value;
			//	echo "ll";
		}else{
			$p=str_replace($key."=".$_GET[$key],$key."=".$value ,$p );
			//	echo "rep";
		}

		return $uri."?".$p;

	}


	//---------------------------
	public function addCss($val){
		$res="";
		foreach($val as $key=>$value){
			$res.='<link rel="stylesheet" type="text/css" href="css/'.$value.'"/>';

		}
		$this->vue->assign_var("ADDCSS",$res );
	}

	//------------------------------
	public function addJs($val){
		$res="";
		foreach($val as $key=>$value){
			$res.='<script type="text/javascript" src="js/'.$value.'"></script>';
		}
		$this->vue->assign_var("ADDJS",$res );
	}




	public function generatePagination($lastpage,$limitpage,$url,$key,$pagesbornes=3){

		if($lastpage==0) return "";
		$Page=Tools::getvalue($key);
		if($Page=="") $Page=1;
		$pagination='<div class="pagination">';
if($Page==1)	{
	$pagination.='<span class="disabled">?</span>';
}else{
	$pagination.='<a href="'.$this->getPaginationLink($Page-1,$key,$url).'">?</a>';
}
		if($lastpage<$limitpage){
			for($i=1;$i<=$lastpage;$i++){
				$pagination.=($Page==$i)? '<span class="current">'.$i.'</span>':'<a href="'.$this->getPaginationLink($i,$key,$url).'">'.$i.'</a>';
			}
		}else{
			for($i=1;$i<=$pagesbornes;$i++){
				$pagination.=($Page==$i)? '<span class="current">'.$i.'</span>':'<a href="'.$this->getPaginationLink($i,$key,$url).'">'.$i.'</a>';
			}
			$pagination.="...";
			$in=$lastpage-$pagesbornes;
			for($j=$in+1;$j<=$lastpage;$j++){
				$pagination.=($Page==$j)? '<span class="current">'.$j.'</span>':'<a href="'.$this->getPaginationLink($j,$key,$url).'">'.$j.'</a>';
			}

		}
		if($Page==$lastpage){
			$pagination.='<span class="disabled">?</span>';
		}else{
			$pagination.='<a href="'.$this->getPaginationLink($Page+1,$key,$url).'">?</a>';
		}
		$pagination.='</div>';

		return $pagination;

	}

	//------------------------------------------------------------------------------

}

?>