<?php
	
	if(! defined('MOOV3G_SOAP_CLIENT_INCLUDED'))
	{
	
		define('MOOV3G_SOAP_CLIENT_INCLUDED', 1) ;
		if(! defined("ASMX_CLIENT_INCLUDED"))
		{
			include dirname(__FILE__)."/AsmxClient.class.php" ;
		}
		
		class Moov3GSoapClient extends AsmxClient
		{
			// var $Url = "http://10.177.18.39:99/Service3.svc?wsdl" ;
			var $Url = "http://localhost:64454/HTTPGateWays/Service3.svc?wsdl" ;
			public $SoapActionEnabled = 1 ;
			public $InterfaceName = "IService3" ;
			public $ParamNamePrefix = "" ;
			public $MethodNamePrefix = "tem" ;
			public $Xmlns = array("moov" => "http://schemas.datacontract.org/2004/07/Moov3G.Old.Message") ;
			function InscriptionMembre($msisdn, $email, $nom='', $prenoms='', $equipement='')
			{
				$node = $this->CallMethod(
					"InscriptionMembre",
					$this->RequestData(
						array(
							'moov:Msisdn' => $msisdn,
							'moov:Email' => $email,
							'moov:Equipement' => $equipement,
							'moov:Nom' => $nom,
							'moov:Prenoms' => $prenoms
						)
					)
				) ;
				return new Moov3GResponseInscriptionMembre($node) ;
			}
			function ActiveBundlePending($msisdn)
			{
				$node = $this->CallMethod(
					"ActiveBundlePending",
					$this->RequestData(
						array(
							'moov:Msisdn' => $msisdn
						)
					)
				) ;
				return new Moov3GResponseActiveBundlePending($node) ;
			}
			function ConfirmBundlePending($msisdn)
			{
				$node = $this->CallMethod(
					"ConfirmBundlePending",
					$this->RequestData(
						array(
							'moov:Msisdn' => $msisdn
						)
					)
				) ;
				return new Moov3GResponseActiveBundlePending($node) ;
			}
			function ActivatePAYG($msisdn)
			{
				$node = $this->CallMethod(
					"ActivatePAYG",
					$this->RequestData(
						array(
							'moov:Msisdn' => $msisdn
						)
					)
				) ;
				return new Moov3GResponseActivatePAYG($node) ;
			}
			function ConnexionMembre($msisdn, $password)
			{
				$node = $this->CallMethod(
					"ConnexionMembre",
					$this->RequestData(
						array('moov:Msisdn' => $msisdn, 'moov:Password' => $password)
					)
				) ;
				return new Moov3GResponseConnexionMembre($node) ;
			}
			function ReinitialisePassword($msisdn, $email)
			{
				$node = $this->CallMethod(
					"ReinitialisePassword",
					$this->RequestData(
						array(
							'moov:Msisdn' => $msisdn,
							'moov:Email' => $email
						)
					)
				) ;
				// print_r($node) ;
				return new Moov3GResponseReinitialisePassword($node) ;
			}
			function ChangePassword($msisdn, $ancienPassword, $nouvPassword)
			{
				$node = $this->CallMethod(
					"ChangePassword",
					$this->RequestData(
						array(
							'moov:Msisdn' => $msisdn,
							'moov:AncienPassword' => $ancienPassword,
							'moov:NouveauPassword' => $nouvPassword,
						)
					)
				) ;
				return new Moov3GResponseChangePassword($node) ;
			}
			function ListeOffre($idConnexion, $idEquipement="SMARTPHONE")
			{
				$node = $this->CallMethod(
					"ListeOffre",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion,
							'moov:IdEquipement' => $idEquipement,
						)
					)
				) ;
				return new Moov3GResponseListeOffre($node) ;
			}
			function StatutAbonne($idConnexion)
			{
				$node = $this->CallMethod(
					"StatutAbonne",
					$this->RequestData(
						array('moov:IdConnexion' => $idConnexion)
					)
				) ;
				return new Moov3GResponseStatutAbonne($node) ;
			}
			function InfoMembre($idConnexion)
			{
				$node = $this->CallMethod(
					"InfoMembre",
					$this->RequestData(
						array('moov:IdConnexion' => $idConnexion)
					)
				) ;
				return new Moov3GResponseInfoMembre($node) ;
			}
			function ModifieMembre($idConnexion, $email, $nom='', $prenoms='', $equipement='')
			{
				$node = $this->CallMethod(
					"ModifieMembre",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion,
							'moov:Email' => $email,
							'moov:Equipement' => $equipement,
							'moov:Nom' => $nom,
							'moov:Prenoms' => $prenoms
						)
					)
				) ;
				return new Moov3GResponseModifieMembre($node) ;
			}
			function SouscritAbonne($idConnexion, $idForfait, $beneficiaire="", $change=false)
			{
				$node = $this->CallMethod(
					"SouscritAbonne",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion,
							'moov:Beneficiaire' => $beneficiaire,
							'moov:Change' => ($change == true) ? "true" : "false",
							'moov:IdForfait' => $idForfait,
						)
					)
				) ;
				return new Moov3GResponseSouscritAbonne($node) ;
			}
			function HistoriqueSouscription($idConnexion, $debutRangee=1, $nombreElementsRangee=5)
			{
				$node = $this->CallMethod(
					"HistoriqueSouscription",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion,
							'moov:DebutRangee' => $debutRangee,
							'moov:NombreElementsRangee' => $nombreElementsRangee,
						)
					)
				) ;
				return new Moov3GResponseHistoriqueSouscription($node) ;
			}
			function ConsultationCompte($idConnexion)
			{
				$node = $this->CallMethod(
					"ConsultationCompte",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion
						)
					)
				) ;
				return new Moov3GResponseConsultationCompte($node) ;
			}
			function RechargementCompte($idConnexion, $codeVoucher)
			{
				$node = $this->CallMethod(
					"RechargementCompte",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion,
							'moov:Code' => $codeVoucher,
						)
					)
				) ;
				return new Moov3GResponseRechargementCompte($node) ;
			}
			function StopSouscription($idConnexion)
			{
				$node = $this->CallMethod(
					"StopSouscription",
					$this->RequestData(
						array(
							'moov:IdConnexion' => $idConnexion,
						)
					)
				) ;
				return new Moov3GResponseStopSouscription($node) ;
			}
			function DeconnexionMembre($idConnexion)
			{
				$node = $this->CallMethod(
					"DeconnexionMembre",
					$this->RequestData(
						array('moov:IdConnexion' => $idConnexion)
					)
				) ;
				return new Moov3GResponseDeconnexionMembre($node) ;
			}
			function ExtractMethodXmlResponse($methodName, & $responseNode)
			{
				if(empty($responseNode))
					return false ;
				if(
					! isset($responseNode[0]["child"])
					|| ! isset($responseNode[0]["child"][0])
					|| $responseNode[0]["child"][0]["name"] != "S:BODY"
					|| ! isset($responseNode[0]["child"][0]["child"])
					|| ! isset($responseNode[0]["child"][0]["child"][0])
					|| $responseNode[0]["child"][0]["child"][0]["name"] != strtoupper($methodName)."RESPONSE"
					|| ! isset($responseNode[0]["child"][0]["child"][0]["child"])
					|| ! isset($responseNode[0]["child"][0]["child"][0]["child"][0])
					|| $responseNode[0]["child"][0]["child"][0]["child"][0]["name"] != strtoupper($methodName)."RESULT"
				)
					return false ;
				$result = array() ;
				if(isset($responseNode[0]["child"][0]["child"][0]["child"][0]["child"]))
				{
					$result = $responseNode[0]["child"][0]["child"][0]["child"][0] ;
				}
				return $result ;
			}
			function RequestData($params=array())
			{
				$data["tem:request"] = array() ;
				foreach($params as $name => $value)
				{
					$data["tem:request"][$name] = $value ;
				}
				return $data ;
			}
		}
		class Moov3GSoapLocalClient extends Moov3GSoapClient
		{
			var $Url = "http://10.177.18.39:99/Service3.svc?wsdl" ;
		}
		class Moov3GSoapWebsiteClient extends Moov3GSoapClient
		{
			var $Url = "http://213.136.123.141:99/Service3.svc?wsdl" ;
		}
		
		class Moov3GResponseBase
		{
			public $AutoImportNode = true ;
			public $ImportableAttrs = array() ;
			public $MessageErreur = "" ;
			public $Succes = "0" ;
			public $LastConfigNode = null ;
			public function __construct($nodeTemp=array())
			{
				if($this->AutoImportNode)
				{
					$this->ImportConfigFromNode($nodeTemp) ;
				}
			}
			function ImportConfigFromNode(&$configNode)
			{
				$this->LastConfigNode = $configNode ;
				if(isset($configNode["attrs"]))
				{
					foreach($configNode["attrs"] as $name => $value)
					{
						if(in_array($name, $this->ImportableAttrs))
						{
							$attrNode = array(
								"name" => $name,
								"content" => $value,
								"child" => array()
							) ;
							$this->ImportConfigMemberFromNode($attrNode) ;
						}
						else
						{
							$this->ImportConfigMemberFromKey($name, $value) ;
						}
					}
				}
				if(isset($configNode["child"]))
				{
					for($i=0; $i<count($configNode["child"]); $i++)
					{
						$this->ImportConfigMemberFromNode($configNode["child"][$i]) ;
					}
				}
				$this->UpdateConfigAfterImport() ;
			}
			function ImportConfigMemberFromKey($name, $value)
			{
				return false ;
			}
			function ImportConfigMemberFromNode(&$node)
			{
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MESSAGEERREUR" :
					{
						$this->MessageErreur = $nodeContent ;
					}
					break ;
					case "A:SUCCES" :
					{
						$this->Succes = ($nodeContent == "true") ? 1 : 0 ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
			function UpdateConfigAfterImport()
			{
			}
		}
		class Moov3GResponseMembreConnecte extends Moov3GResponseBase
		{
			public $Msisdn ;
			public $IdConnexion ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MSISDN" :
					{
						$this->Msisdn = $nodeContent ;
					}
					break ;
					case "A:IDCONNEXION" :
					{
						$this->IdConnexion = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseMembreDeconnecte extends Moov3GResponseBase
		{
			public $Msisdn ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MSISDN" :
					{
						$this->Msisdn = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseInscriptionMembre extends Moov3GResponseMembreDeconnecte
		{
			public $Password = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:PASSWORD" :
					{
						$this->Password = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseDeconnexionMembre extends Moov3GResponseMembreDeconnecte
		{
		}
		class Moov3GResponseChangePassword extends Moov3GResponseBase
		{
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseReinitialisePassword extends Moov3GResponseBase
		{
			public $Password = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:PASSWORD" :
					{
						$this->Password = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseConnexionMembre extends Moov3GResponseMembreDeconnecte
		{
			public $IdConnexion = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:IDCONNEXION" :
					{
						$this->IdConnexion = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseListeOffre extends Moov3GResponseMembreConnecte
		{
			public $TotalElements = "" ;
			public $Elements = array() ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MSISDN" :
					{
						$this->IdConnexion = $nodeContent ;
					}
					break ;
					case "A:TOTALELEMENTS" :
					{
						$this->TotalElements = $nodeContent ;
					}
					break ;
					case "A:ELEMENTS" :
					{
						$this->Elements = array() ;
						if(isset($node["child"]))
						{
							for($j=0; $j<count($node["child"]); $j++)
							{
								$offre = new Moov3GOffre($node["child"][$j]) ;
								$this->Elements[] = $offre ;
							}
						}
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseStatutAbonne extends Moov3GResponseMembreConnecte
		{
			public $ForfaitEnCours = 0 ;
			public $NomForfait ;
			public $VolumeConsomme = 0;
			public $VolumeTotal = 0 ;
			public $DateExpiration = "";
			public $Profil = "";
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:NOMFORFAIT" :
					{
						$this->NomForfait = $nodeContent ;
					}
					break ;
					case "A:PROFIL" :
					{
						$this->Profil = $nodeContent ;
					}
					break ;
					case "A:FORFAITENCOURS" :
					{
						$this->ForfaitEnCours = ($nodeContent == "true") ? 1 : 0 ;
					}
					break ;
					case "A:VOLUMECONSOMME" :
					{
						$this->VolumeConsomme = intval($nodeContent) ;
					}
					break ;
					case "A:VOLUMETOTAL" :
					{
						$this->VolumeTotal = intval($nodeContent) ;
					}
					break ;
					case "A:DATEEXPIRATION" :
					{
						$this->DateExpiration = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseInfoMembre extends Moov3GResponseMembreConnecte
		{
			public $Email = "" ;
			public $Nom = "";
			public $Prenoms = "";
			public $Equipement = "";
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:EMAIL" :
					{
						$this->Email = $nodeContent ;
					}
					break ;
					case "A:NOM" :
					{
						$this->Nom = $nodeContent ;
					}
					break ;
					case "A:PRENOMS" :
					{
						$this->Prenoms = $nodeContent ;
					}
					break ;
					case "A:EQUIPEMENT" :
					{
						$this->Equipement = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseSouscritAbonne extends Moov3GResponseMembreConnecte
		{
			public $MessageSucces = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MESSAGESUCCES" :
					{
						$this->MessageSucces = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		
		class Moov3GResponseActiveBundlePending extends Moov3GResponseBase
		{
			public $Msisdn = "" ;
			public $Statut = false ;
			public $CodeReponse = -11 ;
			public $MessageSucces = "" ;
			public $MessageErreur = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MESSAGESUCCES" :
					{
						$this->MessageSucces = $nodeContent ;
					}
					break ;
					case "A:MESSAGEERREUR" :
					{
						$this->MessageErreur = $nodeContent ;
					}
					break ;
					case "A:CODEREPONSE" :
					{
						$this->CodeReponse = $nodeContent ;
					}
					break ;
					case "A:MSISDN" :
					{
						$this->Msisdn = $nodeContent ;
					}
					break ;
					case "A:STATUT" :
					{
						$this->Statut = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		
		class Moov3GResponseActivatePAYG extends Moov3GResponseActiveBundlePending
		{
			public $MessageSucces = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		
		
		class Moov3GResponseHistoriqueSouscription extends Moov3GResponseMembreConnecte
		{
			public $DebutElement = 0 ;
			public $TotalElements = 0 ;
			public $TotalRangees = 0 ;
			public $Elements = array() ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:TOTALELEMENTS" :
					{
						$this->TotalElements = $nodeContent ;
					}
					break ;
					case "A:DEBUTELEMENT" :
					{
						$this->DebutElement = $nodeContent ;
					}
					break ;
					case "A:TOTALRANGEES" :
					{
						$this->TotalRangees = $nodeContent ;
					}
					break ;
					case "A:ELEMENTS" :
					{
						$this->Elements = array() ;
						if(isset($node["child"]))
						{
							for($j=0; $j<count($node["child"]); $j++)
							{
								$offre = new Moov3GSouscription($node["child"][$j]) ;
								$this->Elements[] = $offre ;
							}
						}
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseRechargementCompte extends Moov3GResponseMembreConnecte
		{
			public $MessageSucces = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MESSAGESUCCES" :
					{
						$this->MessageSucces = preg_replace('/^RESP\:[^\:]+\:/', '', $nodeContent) ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GResponseStopSouscription extends Moov3GResponseRechargementCompte
		{
		}
		class Moov3GResponseModifieMembre extends Moov3GResponseMembreConnecte
		{
		}
		class Moov3GResponseConsultationCompte extends Moov3GResponseMembreConnecte
		{
			public $Montant = "" ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "A:MONTANT" :
					{
						$this->Montant = intval($nodeContent) ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		
		class Moov3GOffre extends Moov3GResponseBase
		{
			public $Id ;
			public $Nom ;
			public $Montant = 0;
			public $TotalJours ;
			public $VolumeMax ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "B:ID" :
					{
						$this->Id = $nodeContent ;
					}
					break ;
					case "B:NOM" :
					{
						$this->Nom = $nodeContent ;
					}
					break ;
					case "B:MONTANT" :
					{
						$this->Montant = $nodeContent ;
					}
					break ;
					case "B:VOLUMEMAX" :
					{
						$this->VolumeMax = $nodeContent ;
					}
					break ;
					case "B:TOTALJOURS" :
					{
						$this->TotalJours = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
		class Moov3GSouscription extends Moov3GResponseBase
		{
			public $NomForfait ;
			public $Montant = 0 ;
			public $DateSouscription ;
			public $DateExpiration ;
			function ImportConfigMemberFromNode(& $node)
			{
				$importSuccess = parent::ImportConfigMemberFromNode($node) ;
				if($importSuccess)
					return true ;
				$importSuccess = true ;
				$nodeContent = (isset($node["content"])) ? $node["content"] : "" ;
				switch($node["name"])
				{
					case "B:NOMFORFAIT" :
					{
						$this->NomForfait = $nodeContent ;
					}
					break ;
					case "B:MONTANT" :
					{
						$this->Montant = $nodeContent ;
					}
					break ;
					case "B:DATESOUSCRIPTION" :
					{
						$this->DateSouscription = $nodeContent ;
					}
					break ;
					case "B:DATEEXPIRATION" :
					{
						$this->DateExpiration = $nodeContent ;
					}
					break ;
					default :
					{
						$importSuccess = false ;
					}
					break ;
				}
				return $importSuccess ;
			}
		}
	}
	
?>